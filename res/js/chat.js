var user_online = Number($('span.uoc').attr('uoc'));//GET THE USER ONLINE 
var profileUser = $("span.profileUser").attr("profileUser"); //GET THE CURRENT PROFILE USER NAME
var jans_clicked = [];

/* REQUEST THINGS */ 
var idsOfCommentsNotSeen = [];
var idsOfLikesNotSeen = [];
var friendsToAccept = [];
/* REQUEST THINGS */ 


function loadFriends(){
    $.ajax({
        url:"/iforum/pages/ajax_pages/usr_m.php",
        type:'post',
        data: {action:'listf',except:user_online},
        dataType:'json',
        success:function(resp){
            if(resp != "") $('.chat #list').html(" ");
            else $('.chat #list').html("<center><small>Nenhum amigo ainda...</small></center>");
            $.each(resp.friends,function(i,f){
        		$('.chat #list').append(f);
            });
            $('.count-friends').html($('.start-conversation').attr('countFriends'));	
        }
    });
}

function checkInitialUsersStatus(user_online){
	$.ajax({
		 url:"/iforum/pages/ajax_pages/usr_m.php",
        type:'post',
        data: {action:'checkInitialUsersStatus',u:user_online}
	});
}

//listar myRequests if needed
var friend_requests = new Array();
function myRequests(){
    $.ajax({
        url:"/iforum/pages/ajax_pages/usr_m.php",
		type:'POST',
        dataType:'json',
		data:{action:'myRequests',user:user_online},
        success:function(resp){
        	$('#friends-requests').html("");
	        $(".friends-top").html("");
	        $('#friends-requests').append("<br><br><center><i style='margin-bottom:10px; font-size:40px; color:#DDD' class='mdi mdi-account-plus'></i><br><small style='font:15px Roboto'>Sem solicitações de amizade...</small></center>");
	    		
        	if(resp.count > 0){$('.count-notifications').html(Number(resp.count)).addClass('nova');}
            else{$('.count-notifications').removeClass('nova')}
                   	
        	if(resp.followRequests != ""){	
        		$('#friends-requests').html("");
        		$(".friends-top").prepend("<i title='aceitar todos' class='mdi mdi-check-all accept-all'></i><br><br>");
	        	for(i in resp.followRequests){
        			if($(".chat #list").find("li#"+resp.followRequests[i].fid+" ").attr('id') == undefined){
                        friendsToAccept.push(resp.followRequests[i].fid);
                        /* SHOW THE REQUEST  */
                        var be = "";
                        if(resp.followRequests[i].sex == "f"){be = "sua amiga";}else if(resp.followRequests[i].sex == "m") {be = "seu amigo";}else if(resp.followRequests[i].sex == "o"){ be = "seu(ua) amigo(a)"; }
                        
                        $('#friends-requests').append("<a href='/iforum/"+resp.followRequests[i].user+"'> <li> <img src="+resp.followRequests[i].img+" width='40' height='40' class='img-circle'><b><a style='color:#666; font-size:13px' href='/iforum/"+resp.followRequests[i].user+"'>  "+resp.followRequests[i].nome+" "+resp.followRequests[i].sobrenome+"</a></b> deseja ser "+be+".   <i title='Excluir "+resp.followRequests[i].nome+" ' fid='"+resp.followRequests[i].fid+"' class='pull-right friend-reject mdi mdi-close'></i> <i title='Aceitar "+resp.followRequests[i].nome+" ' fid='"+resp.followRequests[i].fid+"' class='pull-right friend-accept mdi mdi-check'></i> <br><i class='mdi mdi-account-plus icon-notifications'></i> </li></a>");
                        /* SHOW THE REQUEST  */
                        if(resp.followRequests[i].viewed == 0){
                            /* SHOW THE NOTIFICATION (in anyway)*/
                            var notify = "<div class='notification' id='"+resp.followRequests[i].notification_id+"' ><div class='row'><div class='col-xs-3 col-sm-3 col-md-3'><img src='"+resp.followRequests[i].img+"' width='102' height='100'></div><div class='col-xs-9 col-sm-9 col-md-9'><div class='row header'><div class='col-xs-9 col-sm-9 col-md-9'><a href='/iforum/"+resp.followRequests[i].user+"'><h5 usr='"+resp.followRequests[i].usr+"'><b>"+resp.followRequests[i].nome+" "+resp.followRequests[i].sobrenome+"</b></h5></a></div><div class='col-xs-3 col-sm-3 col-md-3'><span class='pull-right glyphicon glyphicon-remove'></span></div></div><div class='row content'><p> <i style='font-size:16px; color:#20BF90' class='mdi mdi-account-plus'></i> Uma nova solicitação de amizade <!--<b>"+resp.followRequests[i].nome+" "+resp.followRequests[i].sobrenome+"</b>--> <br><br><small class='pull-right'>"+resp.followRequests[i].date+"</small></p></div></div></div></div>";
                            $('.notification-pin').append(notify);
                            /* SHOW THE NOTIFICATION (in anyway)*/
                            friend_requests.push(resp.followRequests[i].notification_id);
                        }
	                }	   
	        	} // end of the for loop
    		} // end of the if (checking if is null) statement 
    		
    		/* LIKES NOT SEEN */
    		if(resp.likesNotSeen != "" || resp.commentsNotSeen != ""){
    			$(".forums-top").html("");
                $("#forums").html("");
                $(".forums-top").prepend("<i title='Visualizar todos...' class='mdi mdi-check-all accept-all'></i><br><br>");
                for(i in resp.likesNotSeen){
                	idsOfLikesNotSeen.push(resp.likesNotSeen[i].id);
                    $('#forums').append("<li notificationType='curtida' id='"+resp.likesNotSeen[i].id+"'  id_postagem='"+resp.likesNotSeen[i].id_postagem+"' categoria_postagem='"+resp.likesNotSeen[i].categoria_postagem+"' class='notify'><img src="+resp.likesNotSeen[i].img+" width='40' height='40' class='img-circle'><b><a style='color:#666; font-size:13px' href='/iforum/"+resp.likesNotSeen[i].user+"'>  "+resp.likesNotSeen[i].name+" "+resp.likesNotSeen[i].sobrenome+"</a></b> curtiu sua dúvida: \""+resp.likesNotSeen[i].duvida_postagem.substring(0,26)+"...\" <br> <i class='mdi mdi-thumb-up icon-notifications'></i></li>");
                }

				/* COMMENTS NOT SEEN */
				for(i in resp.commentsNotSeen){
                	idsOfCommentsNotSeen.push(resp.commentsNotSeen[i].id);
                    $('#forums').append("<li  notificationType='comentario' id='"+resp.commentsNotSeen[i].id+"'  id_postagem='"+resp.commentsNotSeen[i].id_postagem+"' class='notify'><img src="+resp.commentsNotSeen[i].img+" width='40' height='40' class='img-circle'><b><a style='color:#666; font-size:13px' href='/iforum/"+resp.commentsNotSeen[i].user+"'>  "+resp.commentsNotSeen[i].nome+" "+resp.commentsNotSeen[i].sobrenome+"</a></b> respondeu sua dúvida: \""+resp.commentsNotSeen[i].comentario.substring(0,26)+"...\" <br> <i class='mdi mdi-comment icon-notifications'></i></li>");
                }
	    		/* COMMENTS NOT SEEN */

    		}
    		/* LIKES NOT SEEN */
    		

    		$('.notifications  p#friends-title').html("Solicitações ("+Number($('ul#friends-requests').find('li').length)+")");       
        	$('.notifications  p#forums-title').html("Fóruns ("+Number($('ul#forums').find('li').length)+")");
    	}// end of success function.
    });     
}


/* checar seguidor*/
function checkFollow(seguidor,seguido){
	$.ajax({
		url:"/iforum/pages/ajax_pages/usr_m.php",
		type:'post',
        dataType:'json',
		data:{action:'checkfollow',seguidor:seguidor,seguido:seguido},
		success:function(resp){
            if(resp != ""){
            $.each(resp, function(i,r){
                if(r.permission == 1){
                    if(seguidor == r.follower){
                        $('.follow').removeClass('follow').addClass('waiting').html('<i class="mdi mdi-clock icon"></i> Esperando');
                    }
                    else{ $(".friendsdiv").html("<div class='btn-group'><button type='button' class='btn btn-success'>Responder solicitação </button><button type='button' class='btn dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span class='caret'></span><span class='sr-only'></span></button><ul class='dropdown-menu'> <li fid='"+r.follower+"' class='friend-accept'><a><i class='mdi mdi-account-check' style='font-size:16px;color:#666;'></i> Aceitar</a></li><li class='friend-reject'><a><i class='mdi mdi-delete' style='font-size:16px;color:#666;'></i> Excluir</a></li></ul></div>");}
                }
                else if(r.permission == 2){
                    $('.follow').removeClass('waiting').removeClass('follow').addClass('follow_done').html('<i class="mdi mdi-account-multiple icon"></i> Amigos');
                }
                else{
                    $('.follow').removeClass('waiting').removeClass('follow').addClass('follow_done').html('<span class="mdi mdi-account icon"></span> Amigos');
                }
            });
		  }else{
               $('.follow').removeClass('unfollow').addClass('follow').html('<i class="mdi mdi-account-plus icon"></i> Solicitar amizade');
          }
        }
	});
}
/* checar seguidor*/
context = $(function(){
	/*ONLOAD THE PAGE*/
	loadFriends();
   	qtdForums = (context.children().children('body').attr('id') == 'body-forum') ? 3 : 1;
	loadPosts(qtdForums);	   	
   	myRequests();
   	checkInitialUsersStatus(user_online);
   	/*ONLOAD THE PAGE*/

   	/* ANIMATION TESTS */

   	$("#navbar-main button").click(function(){
   		if($(this).hasClass('anim')){$(this).removeClass('anim');}else{$(this).addClass('anim');}
   		$(".hideIt").toggle(200);
   	});
   	
   	$("#navbar-main button").blur(function(){
   		if($(this).hasClass('anim')){$('.hideIt').toggle(200);$(this).removeClass('anim');}	
   	});	
   	/* ANIMATION TESTS */


    function in_array(value, array){for (var i = 0; i < array.length; i++) {if(value == array[i]){return true;}}return false;}

	function createWindow(id,user,nome,status){
		var janelas = Number($("#janelas .chat-popup").length);
		var pixels  = (260+7)*janelas;
		var style 	= "float:none;position:absolute;bottom:0;right:"+pixels+"px";
		var popup = "<div id='jan_"+id+"' class='chat-popup' style='"+style+"'><div class='chat-popup-top'  id='"+id+"'><div class='row'><div class='col-xs-6  col-sm-6  col-md-10'> <!--<status id='status-chat-popup' class='status-off' style='margin-left:15px; margin-right:-1px; font-size:14px;'>o</status>--><n id='chat-user-conversation' user='"+user+"'>"+nome+"</n></div><div class='col-xs-6 col-sm-6 col-md-2'><span class='chat-popup-close glyphicon glyphicon-remove pull-right'> </span></div></div></div><div class='chat-body'><div class='mensagens'><div class='listar'><ul>  </ul></div></div><textarea id='"+id+"' class='mensagem' placeholder='Digite algo...' autofocus='true' ></textarea><span class='menu-options glyphicon glyphicon-option-vertical'></span></div></div>";
        $('#janelas').append(popup);
	}
	function return_history(id_to){
		$.ajax({
			type:"POST",
			url :"/iforum/pages/ajax_pages/usr_m.php",
			data:{action:'history', to:id_to,from:user_online},
			dataType:'json',
			success:function(resp){
				msgViewed = 0;
				$.each(resp,function(i,msg){
					if(msg.message != 'nothing'){
						msgViewed = msg.lastMessageWasSeen;
						if($('#jan_'+msg.id_jan).length != 0){
							if(user_online == msg.id_from){
								$("#jan_"+msg.id_jan+" .listar ul").append("<li id="+msg.id+" class='from'><div class='arrow'></div><span title='"+msg.datahora+"'>"+msg.message+"</span></li>");
							}else{
								$("#jan_"+msg.id_jan+" .listar ul").append("<li id="+msg.id+" class='to'><div class='arrow'></div><span title='"+msg.datahora+"'><img title="+msg.nome+" src="+msg.fotoUser+" class='img-circle toImg'>"+msg.message+"</span></li>");
							}
						}
					}else{
						$("#jan_"+id_to+" .listar").html("<center><br><br><br><br><br><br><small>Você não tem conversa com "+msg.user+"</small></center>");
					}

				});
				// This code set the messagers with the reverse order...
				[].reverse.call($('#jan_'+id_to+" .listar li")).appendTo('#jan_'+id_to+" .listar ul");
				$("#jan_"+id_to+" .listar").animate({scrollTop:$('#jan_'+id_to+" .listar ul").height()},200);
				
				//Visualizado...
				if(msgViewed == 1){
					$('#jan_'+id_to+' .mensagens').append("<div class='msgViewed'><small style='margin-left:170px;color:#999; font-size:11px'><i class='mdi mdi-check-all'></i> Visualizado</small></div>");
				}else{
					$('#jan_'+id_to+' .mensagens').append("<div class='msgViewed'><small style='margin-left:170px;color:#999; font-size:11px'> </small></div>");
				}
			}
		});
	}

	$('body').on("click",".start-conversation",function(){
		var id 		= $(this).attr('id_comp');
		var nome 	= $(this).attr('nome');
        var user 	= $(this).attr('user');
        var status  = $(".friend").attr('class');
		var idTo 	= id.split(':');
		var idJan   = Number(idTo[1]);

		$(this).removeClass('start-conversation');

		if($("#jan_"+idJan).length == 0){
			createWindow(idJan,user,nome,status);
			return_history(idJan);

		}else{
			$(this).removeClass('start-conversation');
		}
	});

	//function to minimize the chat-popup
    aux = 0;
	p = 0;
    $('body').on('click','.chat-popup-top',function(){
        $(this).next().toggle(100);
		id = $(this).attr('id');
		aux = $('#jan_'+id+" .listar ul").height();

        if(aux > 100){
			p = $('#jan_'+id+" .listar ul").height();
		}
		$("#jan_"+id+" .listar").animate({scrollTop: p},200);
	});
    
    
    
    
	//function to close the chat-popup
	$('body').on('click','.chat-popup-close',function(){
		pai 		= $(this).parent().parent().parent().parent();
		aux 		= pai.attr('id').split('_');
		idJan 		= Number(aux[1]);

		contagem  	= Number($('.chat-popup').length)-1;
		indice 		= Number($('.chat-popup-close').index(this));
		restantes 	= contagem-indice;

		for (var i = 1; i <= restantes; i++) {
			$('.chat-popup:eq('+(indice+i)+')').animate({right:"-=260"},200);
		}
		pai.remove();
		$(".chat #list li#"+idJan).addClass('start-conversation');
		//return false;
	});


    //Open the profile...
    $('body').on('click','#chat-user-conversation',function(){
		location.href = "/iforum/"+$(this).attr('user');
		return false;
	});


	/* SEND MESSAGE */

	$('body').on('keyup','.mensagem',function(e){
		campo = $(this);
		if(e.which == 13){
			var msg 	= campo.val();
			var toUser 	= Number(campo.attr('id'));
			$('#jan_'+toUser+' .mensagens .msgViewed small').html("");
			if($.trim(msg).length != 0){
				$.ajax({
					type:'POST',
					url :'/iforum/pages/ajax_pages/usr_m.php',
					data:{action:'send',message:msg,from:user_online,to:toUser},
					success: function(resp){
				        campo.val("");
				        // CLOSE AND REOPEN (TO 'FIX' A LITTLE BUG)
				        if(resp == 1){$("#jan_"+campo.attr('id')+" .chat-popup-close").click();$('.start-conversation#'+toUser).click();}		
					}
				});
			}
		}else if(e.which == 27){ 
			if($.trim($(this).val()).length > 0){
				if(confirm("Tem certeza que deseja fechar a jenela?")){
					$("#jan_"+campo.attr('id')+" .chat-popup-close").click();
				}
			}else{
				$("#jan_"+campo.attr('id')+" .chat-popup-close").click();
			}
		}
	});
	
	$(".search_user").keyup(function(e){
		n = $(this).val();
		$.ajax({
			type:"POST",
			url :"/iforum/pages/ajax_pages/usr_m.php",
			data:{action:"listf",usr:n,except:user_online},
			dataType:'json',
			success:function(r){
				 if(r.friends != undefined){
					$('.chat #list').html(r.friends);
				 }else{
				 	$('.chat #list').html("<center><small>Amigo(a) não encontrado...</small></center>");
				 }
			}
		});
	});

	$('body').on('click','.chat-popup',function(){
		id_to = $(this).attr('id').split('_')[1];
		$.ajax({
			url:"/iforum/pages/ajax_pages/usr_m.php",
			type:'post',
			data:{action:'view',id_to:id_to,id_from:user_online}
		});
	});


	/* FOLLOW ACTIONS */
	$('body').on('click','.follow',function(){
		$(this).removeClass('follow').addClass('waiting').html("<i class='mdi mdi-clock icon'></i> Esperando");
		id = $(this).attr('id').split(':');
		seguidor 	= id[0];
		seguido 	= id[1];
		$.ajax({
			url:"/iforum/pages/ajax_pages/usr_m.php",
			type:'post',
			data:{action:'follow',seguidor:seguidor,seguido:seguido}
		});

	});
	$('body').on('click','.waiting',function(){
		$(this).removeClass('waiting').addClass('follow').html("<span class='glyphicon glyphicon-user icon'></span> Solicitar amizade");
		id = $(this).attr('id').split(':');
		seguidor 	= id[0];
		seguido 	= id[1];
		$.ajax({
			url:"/iforum/pages/ajax_pages/usr_m.php",
			type:'post',
			data:{action:'unfollow',seguidor:seguidor,seguido:seguido}
		});

	});

	$('body').on('mouseover','.waiting',function(){
		$(this).removeClass('waiting').addClass('unfollow_w').html("<i class='mdi mdi-delete icon'></i> Cancelar solicitação");
	});
	$('body').on('mouseleave','.unfollow_w',function(){
		$(this).removeClass('unfollow_w').addClass('waiting').html("<i class='mdi mdi-clock icon'></i> Esperando ");
	});

	$('body').on('mouseover','.follow_done',function(){
		$(this).removeClass('follow_done').addClass('unfollow').html("<i class='mdi mdi-delete icon'></i> Remover amizade");
	});
	$('body').on('mouseleave','.unfollow',function(){
		$(this).removeClass('unfollow').addClass('follow_done').html("<i class='mdi mdi-account-multiple icon'></span> Amigos");
	});



	$('body').on('click','.unfollow',function(){
		$(this).removeClass('unfollow').removeClass('follow_done').addClass('follow').html("<i class='mdi mdi-account-plus icon'></i> Solicitar amizade");
		id = $(this).attr('id').split(':');
		seguidor 	= id[0];
		seguido 	= id[1];
		$.ajax({
			url:"/iforum/pages/ajax_pages/usr_m.php",
			type:'post',
			data:{action:'unfollow',seguidor:seguidor,seguido:seguido},
            success:function(){
                loadFriends();
            }
		});
	});
	$('body').on('click','.unfollow_w',function(){
		$(this).removeClass('unfollow_w').addClass('follow').removeClass('follow_done').html("<i class='mdi mdi-account-plus icon'></i> Solicitar amizade");
		id = $(this).attr('id').split(':');
		seguidor 	= id[0];
		seguido 	= id[1];
		$.ajax({
			url:"/iforum/pages/ajax_pages/usr_m.php",
			type:'post',
			data:{action:'unfollow',seguidor:seguidor,seguido:seguido}
		});
	});


    $("body").on('click','.friend-accept',function(){
        var follower = Number($(this).attr('fid'));
        $.ajax({
            url:'/iforum/pages/ajax_pages/usr_m.php',
            type:'post',
            data:{action:'accept-following', follower:follower,followed:user_online},
            success:function(){
                $('.friendsdiv').html("<button id='"+user_online+":"+follower+"' class=' follow btn btn-follow'><i class='mdi mdi-account-multiple'></i> Amigos</button>");
                loadFriends();
	            myRequests();
            }
        });
    });


    $("body").on('click','.friends-top .accept-all',function(){
        $.ajax({url:'/iforum/pages/ajax_pages/usr_m.php',type:'post',data:{action:'accept-following',listFriends:friendsToAccept,followed:user_online},
            success:function(){location.href = "/iforum/"+profileUser+"/notificacoes";	 }
        });
    });
    

    
    setTimeout(close_notification,2500);
    function close_notification(){
        for(i in friend_requests){
            $.ajax({url:"/iforum/pages/ajax_pages/usr_m.php",
                type:'post',
                data:{action:'view_friend_notify',id:friend_requests[i]}
            });
            $('.notification#'+friend_requests[i]).fadeOut(1000);
        }
    }
	/* FOLLOW ACTIONS */




    $('body').on('click','.notification',function(){
        window.location = '/iforum/'+$('span.uoc').attr('myUsr')+'/notificacoes';
    });

    $('body').on('click','#forums .notify',function(){
    	id 		= $(this).attr('id');
    	post    = "/iforum/salas/"+$(this).attr('categoria_postagem')+"/"+$(this).attr('id_postagem');
    	notificationType = $(this).attr('notificationType');
    	$.ajax({
    		url:'/iforum/pages/ajax_pages/usr_p.php',
    		type:'POST',
    		data:{action:'seeRequests', id:id, notificationType:notificationType},
    		success:function(){
    			location.href = post; 	
    		}
    	});
    });


    $('body').on('click','.forums-top .accept-all',function(){
    	$.ajax({url:"/iforum/pages/ajax_pages/usr_p.php",type:"post",data:{action:'seeRequests', likes:idsOfLikesNotSeen, comments:idsOfCommentsNotSeen},
			success:function(){
				location.href = "/iforum/"+profileUser+"/notificacoes";	
			}
    	});
    });

	function longpolling(timestamp, lastid, user){
		var t;
        $.ajax({
			url: '/iforum/pages/ajax_pages/stream.php',
			type:'GET',
			data: "timestamp="+timestamp+"&lastid="+lastid+"&user="+user,
			dataType:'json',
				success:function(resp){
                    clearInterval(t);
                    
                    if(resp.countNotifications > 0){$('.count-notifications').html(Number(resp.countNotifications)).addClass('nova');}
                    else{$('.count-notifications').removeClass('nova')}
                    /* FOLLOWERS */
                    $('#friends-requests').html("");
                    $(".friends-top").html("");
                    if(resp.followers != ""){
                        $(".friends-top").prepend("<i title='Aceitar todos...' class='mdi mdi-check-all accept-all'></i><br><br>");
                        for(i in resp.followers){
                            if($(".chat #list").find("li#"+resp.followers[i].fid+" ").attr('id') == undefined){
                                var be = "";
                                if(resp.followers[i].sex == "f"){be = "sua amiga";}else if(resp.followers[i].sex == "m") {be = "seu amigo";}else if(resp.followers[i].sex == "o"){ be = "seu(ua) amigo(a)"; }
                                $('#friends-requests').append("<li><img src="+resp.followers[i].img+" width='40' height='40' class='img-circle'><b><a style='color:#666; font-size:13px' href='/iforum/"+resp.followers[i].user+"'>  "+resp.followers[i].nome+" "+resp.followers[i].sobrenome+"</a></b> deseja ser "+be+".  <i title='Excluir "+resp.followers[i].nome+" ' fid='"+resp.followers[i].fid+"' class='pull-right friend-reject mdi mdi-close'>  </i>  <i title='Aceitar "+resp.followers[i].nome+" ' fid='"+resp.followers[i].fid+"' class='pull-right friend-accept mdi mdi-check'></i>  <br><i class='mdi mdi-account-plus icon-notifications'></i> </li>");
                            	friendsToAccept.push(resp.followers[i].fid);
                            }
                        }
                    }else{
                        $('#friends-requests').append("<br><br><center><i style='margin-bottom:10px; font-size:40px; color:#DDD' class='mdi mdi-account-plus'></i><br><small style='font:15px Roboto;'>Sem solicitações de amizade...</small></center>");
                    }
                    /* FOLLOWERS */
                    
                    /* LIKES NOT SEEN */
                    if(resp.likesNotSeen != ""){
                    	$(".forums-top").html("");
                    	$("#forums").html("");
                    	$(".forums-top").prepend("<i title='Visualizar todos...' class='mdi mdi-check-all accept-all'></i><br><br>");
                    	for(i in resp.likesNotSeen){
                    		idsOfLikesNotSeen.push(resp.likesNotSeen[i].id);
                    		$('#forums').append("<li notificationType='curtida' id='"+resp.likesNotSeen[i].id+"'  id_postagem='"+resp.likesNotSeen[i].id_postagem+"' categoria_postagem='"+resp.likesNotSeen[i].categoria_postagem+"' class='notify'><img src="+resp.likesNotSeen[i].img+" width='40' height='40' class='img-circle'><b><a style='color:#666; font-size:13px' href='/iforum/"+resp.likesNotSeen[i].user+"'>  "+resp.likesNotSeen[i].name+" "+resp.likesNotSeen[i].sobrenome+"</a></b> curtiu sua dúvida: \""+resp.likesNotSeen[i].duvida_postagem.substring(0,26)+"...\" <br> <i class='mdi mdi-thumb-up icon-notifications'></i> </li>");
                    	}
                    }

                    /* COMMENTS NOT SEEN */
					for(i in resp.commentsNotSeen){
	                	idsOfCommentsNotSeen.push(resp.commentsNotSeen[i].id);
	                    $('#forums').append("<li notificationType='comentario' id='"+resp.commentsNotSeen[i].id+"'  id_postagem='"+resp.commentsNotSeen[i].id_postagem+"' class='notify'><img src="+resp.commentsNotSeen[i].img+" width='40' height='40' class='img-circle'><b><a style='color:#666; font-size:13px' href='/iforum/"+resp.commentsNotSeen[i].user+"'>  "+resp.commentsNotSeen[i].nome+" "+resp.commentsNotSeen[i].sobrenome+"</a></b> respondeu sua dúvida: \""+resp.commentsNotSeen[i].comentario.substring(0,26)+"...\" <br> <i class='mdi mdi-comment icon-notifications'></i></li>");
	                }
		    		/* COMMENTS NOT SEEN */


                    /* LIKES NOT SEEN */
                   	

                   	$('.notifications  p#friends-title').html("Solicitações ("+Number($('ul#friends-requests').find('li').length)+")");
                    $('.notifications  p#forums-title').html("Fóruns ("+Number($('ul#forums').find('li').length)+")");
                    

					if(resp.status == 'ok' || resp.status == 'void'){
						t = setTimeout(function(){
							longpolling(resp.timestamp,resp.lastid,user_online);
						},1000);
						if(resp.status == 'ok'){
							$.each(resp.dados,function(i,msg){
                            	if($("#jan_"+msg.jan_id).length == 0 && msg.id_to == user_online){
									$('.start-conversation#'+msg.jan_id).click();
									jans_clicked.push(msg.jan_id);
								}
								if(!in_array(msg.jan_id, jans_clicked)){
                                    if($('.listar li#'+msg.id).length == 0 && msg.jan_id > 0) {

                                        if(user_online == msg.id_from){
											$("#jan_"+msg.jan_id+" .listar ul").append("<li id="+msg.id+" class='from'><div class='arrow'></div><span title='"+(msg.datahora)+"'>"+msg.message  +"  </span></li>");
                                        }else{
											$("#jan_"+msg.jan_id+" .listar ul").append("<li id="+msg.id+" class='to'><div class='arrow'></div><span title='"+(msg.datahora)+"'><img title="+msg.nome+" src="+msg.fotoUser+" class='img-circle toImg'>"+msg.message+"</span></li>");
										}
                                    }
								}else{
									//Play the sound
									if(msg.id_to == user_online){$.playSound('/iforum/res/audio/here.mp3');}
								}
							});
							h = $('#jan_'+resp.jan_id+" .listar ul").height();
							$("#jan_"+resp.jan_id+" .listar").animate({scrollTop:h},200);

							
							
						}
						jans_clicked = [];
						$.each(resp.users,function(i,user){
							$("li#"+user.id+" .friend").removeClass('status-off').addClass(user.status);
						});
					}else if(resp.status == "erro"){
						alert('Ficamos confusos... Atualize a página!');
					}
				},
				error:function(resp){
					clearInterval(t);
					t = setTimeout(function(){
						longpolling(resp.timestamp,resp.lastid,user_online);
					},15000);
				}
			});
	}

	longpolling(0,0,user_online);


    /* EDIT PROFILE DATA  */
    $('body').on('click','.edit-profile-data',function(){location.href = "/iforum/"+profileUser+"/editar";});
    /* EDIT PROFILE DATA  */

    /*  ERASE ALL DATA OF A USER */
    $('body').on('click','#erase-all-my-data',function(){
        $('#areusure').modal('show');
        $('#yes').click(function(){
            $.ajax({
                url:"/iforum/controller/usuario.php",
                type:'post',
                data:{'xERASEALLDATAx':'ok',u:user_online},
                success:function(resp){
                }
            });
        });
    });
    /*  ERASE ALL DATA OF A USER */

});
