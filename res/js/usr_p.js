var user_online = Number($('span.uoc').attr('uoc'));//GET THE USER ONLINE
var duvida  = null; comentario = null; post = false; 
var posts   = new Array();
var foruns  = new Array();

function loadForuns(materia){
    $.ajax({
        type:'POST',url:'/iforum/pages/ajax_pages/usr_p.php',
        data:{action:'listforuns',materia:materia},
        dataType:'json',
        success:function(resp){
            $('.foruns').html(" ");
            $.each(resp,function(i,f){
                $('.foruns').append(f.forum);
                foruns.push(f.id); 
            });
        }
    });
}


$('.dashboard-main').on('click','.loadmore',function(){    
    limit = $(".dashboard-main").find(".post").length+3;
    $.ajax({
        type:'POST',url:'/iforum/pages/ajax_pages/usr_p.php',
        data:{action:'list',limit:limit},
        dataType:'json',
        success:function(resp){
            remaining = 0;
            posts = new Array();
            $('.posts').html(" ");
            $.each(resp,function(i,p){
                posts.push(p.id);      
                $('.posts').append(p.post);
                remaining = p.remaining;
                $('.comentarios').hide();
            });
            loadComments();
            loadLikes(posts);
            if(remaining <= 0){$('.dashboard-main .loadmore').remove()}
        } 
    });
});



function loadPosts(limit,speedToShow){
    
    $.ajax({
        type:'POST',url:'/iforum/pages/ajax_pages/usr_p.php',
        data:{action:'list',limit:limit},
        dataType:'json',
        success:function(resp){
            remaining = 0;
            posts = new Array() ;
            $('.posts').html(" ");
            $(".posts").hide();
            $.each(resp,function(i,p){
                posts.push(p.id);      
                $('.posts').append(p.post);
                $('.count-posts').html(p.count);
                remaining = p.remaining;
                $('.comentarios').hide();
            });
            $(".posts").toggle(speedToShow);   
            loadComments();
            loadLikes(posts);
            if(remaining > 0){ $('.dashboard-main .loadmore').remove(); $('.dashboard-main').append("<center><button style='margin-top:25px' class='btn btn-default loadmore  '>Carregar mais</button></center>");}
        }
    });
}


function loadLikes(posts){
    for(i = 0; i < posts.length; i++){
        likes     = $('.posts').find('.post#'+posts[i]).attr('likes');
        likedbyme = $('.posts').find('.post#'+posts[i]).attr('likedbyme');
        if(likedbyme == 1){
            $('.post#'+posts[i]+' .do_like').css("color","#2FAF70");
        }else{
            $('.post#'+posts[i]+' .do_like').css("color","#AAA");
        }
        if(likes == 1) $('.post#'+posts[i]+' .count_likes').html(" "+likes+" curtida");else $('.post#'+posts[i]+' .count_likes').html(" "+likes+" curtidas");
    }
}


function loadComments(){
    $.ajax({
        type:'POST',url:'/iforum/pages/ajax_pages/usr_p.php',
        data:{action:'list_c',list_posts:posts},dataType:'json',
        success:function(resp){                        
            $('.post .cx-comments .comments').html('<center><small class="remove-null-note">Nenhuma resposta ainda...</small></center>'); 
            $('.post .count-comentarios').html(' 0 respostas');
            
            $.each(resp,function(i,comment){
                $(".post#"+comment.id_post+" small.remove-null-note").addClass('hide');
                $(".post#"+comment.id_post+" .cx-comments .comments").append(comment.comment);
                if($(".post#"+comment.id_post).find('.comment').length == 1){
                    $(".post#"+comment.id_post).find('.count-comentarios').html(" "+$(".post#"+comment.id_post).find('.comment').length+" resposta");                                                                                            
                }else{
                    $(".post#"+comment.id_post).find('.count-comentarios').html(" "+$(".post#"+comment.id_post).find('.comment').length+" respostas");                                                                                            
                }
                star = $(".post#"+comment.id_post).attr('stared');
                $(".post#"+comment.id_post+" .cx-comments  .comment#"+star+" .star-this-comment").removeClass('glyphicon-star-empty').addClass('glyphicon-star stared');
            });
        }
    });
}   

$(document).ready(function(){	
    
    function edit(comp,element){
        main = $(element);
        id   = main.attr('id_'+comp);
        placeToEdit = $('.'+comp+'#'+id+' .edit-area-'+comp);
        main.removeClass('edit-'+comp);
        
        if(comp == 'post'){
            textarea    = "<div class='container-fluid'> <textarea class='edit-area-"+comp+"' autofocus style='font:15px Roboto;'>"+placeToEdit.children().text()+"</textarea> <div class='row'> <div class='col-xs-6 col-sm-6 col-md-6'></div> <div class='col-xs-6 col-sm-6 col-md-6'> <button class='btn  cancel-edit'>Cancelar</button><button class='btn pull-right save-edit'>Salvar</button></div></div></div>"; 
        }else{
            textarea    = "<div class='container-fluid'> <textarea class='edit-area-"+comp+"' autofocus style='font:13px Roboto;'>"+placeToEdit.children().text()+"</textarea> <div class='row'> <div class='col-xs-6 col-sm-6 col-md-6'></div> <div class='col-xs-6 col-sm-6 col-md-6'> <button class='btn  cancel-edit'>Cancelar</button><button class='btn pull-right save-edit'>Salvar</button></div></div></div>"; 
        }
        
        placeToEdit.html(textarea);
        val = $('.'+comp+'#'+id+' .edit-area-'+comp).children().children().val();            

        $('body').on('click','.cancel-edit',function(){
            if(comp == 'post'){
                placeToEdit.html("<p style='font: 17px Roboto;color:#5F5F5F; text-align:justify'>"+val+"</p>");
            }else{
                placeToEdit.html("<p style='font: 13px Roboto;color:#5F5F5F; text-align:justify'>"+val+"</p>");
            }
            main.addClass('edit-'+comp);
        });   

        $('body').on('click','.save-edit',function(){
            val = $('.'+comp+'#'+id+' .edit-area-'+comp).children().children().val();            
            $.ajax({
                url:'/iforum/pages/ajax_pages/usr_p.php',
                type:'post',
                data:{action:'update-'+comp,id_edited:id,val:val},
                success:function(resp){
                    if(comp == 'post'){
                        placeToEdit.html("<p style='font: 17px Roboto;color:#5F5F5F; text-align:justify'>"+val+"</p>");
                    }else{
                        placeToEdit.html("<p style='font: 13px Roboto;color:#5F5F5F; text-align:justify'>"+val+"</p>");
                    }
                    main.addClass('edit-'+comp);
                }
            });
        });   
    }



    $('body').on('click','.dropdown-menu .edit-post',function(){
        edit('post',$(this));
    });   

    $('body').on('click','.dropdown-menu .edit-comment',function(){
        edit('comment',$(this));
    });    
    


    $("body").on("click",".dropdown-menu .delete-post",function(){
        var id = $(this).attr('id_post');
        $('#areusure').modal('show');
        $('body').on('click','#areusure #yes',function(){
            $.ajax({type:'POST',url:"/iforum/pages/ajax_pages/usr_p.php",data:{action:'del',id:id},success:function(){setTimeout(loadPosts(3),1000);}});
        });
    });



    /* FUNÇÕES DE VERIFICAÇÃO DA POSTAGEM*/
    
    function postar(duvida,categoria){
        $.ajax({type:'POST',url:'/iforum/pages/ajax_pages/usr_p.php',data:{action:'post',duvida: duvida,categoria: categoria,id_user:user_online},success:function(){ loadPosts(3,300) }});    
    }
    function limparPost(){
        $("#duvida").val("").animate({height:34},200);
        $("select#categs")[0].selectedIndex = 0;
        $("#btn-publicar").css("background","#AFDFBF");
    }
    $("#btn-publicar").click(function(){
        category = $("select#categs").val();
        content  = $("textarea#duvida").val();    
        if(($.trim(category).length) > 0  && ($.trim(content).length) > 0 ){
            postar(content,category);
            limparPost();
        }else{
            if(($.trim(content).length) == 0){
                $('textarea#duvida').popover();
                $('textarea#duvida').popover('show');
                setTimeout("$('textarea#duvida').popover('destroy')",4000);
            }
            else if(($.trim(category).length) == 0  ){
                $('select#categs').popover();
                $('select#categs').popover('show');
                setTimeout("$('select#categs').popover('destroy')",4000);
            }
        }        
    });
    /* SÓ ESTETICA */
    $("textarea#duvida").keyup(function() {
        if($.trim($(this).val()).length > 0 && $.trim($('select#categs').val().length) > 0){
            $("#btn-publicar").css("background","#2FC080");            
        }else{
            $("#btn-publicar").css("background","#AFDFBF");
        }
    });
    $('select#categs').change(function(){
       if($.trim($(this).val()).length > 0 && $.trim($('select#categs').val().length) > 0){
            $("#btn-publicar").css("background","#2FC080");            
        }else{
            $("#btn-publicar").css("background","#AFDFBF");
        } 
    });
    /* SÓ ESTETICA */


    


    $('body').on('click','.do_like',function(){
        obj = $(this);
        id_post = obj.parent().parent().parent().parent().parent().attr("id");
        $.ajax({type:'POST',url:"/iforum/pages/ajax_pages/usr_p.php",data:{action:'like',id_post:id_post,id_user:user_online},
            success:function(resp){
                if(resp == "liked"){
                    $(obj).css("color","#2FAF70");
                    likes = Number($(".post#"+id_post).find('.count_likes').text().split(" ")[1]);
                    if(likes+1 == 1){$(".post#"+id_post+" .count_likes").html(" "+(likes+1)+" curtida");}else{$(".post#"+id_post+" .count_likes").html(" "+(likes+1)+" curtidas");}
                }else{
                    $(obj).css("color","#AAA");
                    likes = Number($(".post#"+id_post).find('.count_likes').text().split(" ")[1]);
                    if(likes-1 == 1){$(".post#"+id_post+" .count_likes").html(" "+(likes-1)+" curtida");}else{$(".post#"+id_post+" .count_likes").html(" "+(likes-1)+" curtidas");}
                }
            }
        });    
    });
    $('body').on('click','.count_likes',function(){
        obj = $(this);
        id_post = obj.parent().parent().parent().parent().parent().attr("id");
        $.ajax({
            type:'POST',
            dataType:'json',
            url:'/iforum/pages/ajax_pages/usr_p.php',
            data:{action:'peopleLikedMyPost',id:id_post},
            success:function(resp){
                if(resp != ""){
                    content = "";
                    $('#peopleLikedModal').modal('show');                
                    $.each(resp,function(i,p){content = p.person;});
                    $('#peopleLikedModal .listPeople').html(content);    
                }
            }
        });
    });


    $('body').on('click','.send_comment',function(){
        var field   = $(this).parent().parent().children('.col-md-11').children('textarea');
        var comment = field.val();
        var id_post = $(this).parent().children('#id_post').val();
        if($.trim(comment).length > 0){
            $.ajax({type:'POST',url:'/iforum/pages/ajax_pages/usr_p.php',data:{action:'comment',comment:comment,id_post,id_post}});
            $(field).val("");
            setTimeout(loadComments,100);
            $(".post#"+id+" .cx-comments").animate({scrollTop:$(".post#"+id+" .cx-comments .comments").height()},50);
        }else{
            alert("Diga algo...");
        }
    });
    
                                        
    $('body').on('click','.delete-comment',function(){
        var id = $(this).attr('id_comment');
        $('#areusure').modal('show');
        $('body').on('click','#areusure #yes',function(){
            $.ajax({type:'POST',url:'/iforum/pages/ajax_pages/usr_p.php',data:{action:'del_c',id_c:id},success:function(){ setTimeout(loadComments,1000) }});
        });
        
    });
    

    $('body').on('click','.comment-icon',function(){
        $(this).parent().parent().parent().children(".comentarios").toggle(400); 
        id = $(this).parent().parent().parent().parent().parent().attr('id');
        $(".post#"+id+" .cx-comments ").animate({scrollTop:$(".post#"+id+" .cx-comments .comments").height()},1000);
    });
    $('body').on('click','.hide_comments',function(){
         $(this).parent().parent().parent().parent().parent().hide(400); 
    });

    /* MELHOR RESPOSTA */
    $('body').on('click','.star-this-comment',function(){
        id_comment = $(this).attr('id');
        id_post    = $(this).attr('id_post');
        $(".post#"+id_post+" .comment .star-this-comment").removeClass('stared');
        $(this).addClass('stared');
        $.ajax({url:'/iforum/pages/ajax_pages/usr_p.php',type:'post',data:{action:'star', id_comment:id_comment,id_post:id_post}});
    });
    
});