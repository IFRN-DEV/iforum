<?php 
	require_once ('Connection.php');
	$root =  $_SERVER['DOCUMENT_ROOT'];
	require_once ($root."/iforum/model/Mensagem.php");
	class MensagemDAO extends Connection{
		public static function enviar($m){
			$check = 0;
			try {
	            $query  = "INSERT INTO mensagem (id_to,id_from,mensagem,data_hora,time,lido) VALUES(?,?,?,?,?,?)";
	            $stmt            = Connection::prepare($query);
	            $stmt->bindValue(1, $m->getId_to());
	            $stmt->bindValue(2, $m->getId_from());
	            $stmt->bindValue(3, $m->getMensagem());
	            $stmt->bindValue(4, $m->getDataHora());
	            $stmt->bindValue(5, time());
	            $stmt->bindValue(6, 0);       
	            $check = $stmt->execute();
	           	
	        } catch (PDOException $e) {
	            echo "ERRO AO ENVIAR MENSAGEM (MensagemDAO)\n".$e->getMessage();
	        }
	        return $check;
		}
		public static function listAllMsgs($id_from,$id_to,$limit){
			$res = null; 
			try {
				$query 	= "SELECT * FROM mensagem WHERE id_from = ? AND id_to = ? OR id_from = ? AND id_to = ? ORDER BY id DESC LIMIT $limit";					
				$stmt 	= Connection::prepare($query);
				
				$stmt->bindValue(1,$id_from);
				$stmt->bindValue(2,$id_to);
				$stmt->bindValue(3,$id_to);
				$stmt->bindValue(4,$id_from);
				
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER LISTAGEM (MensagemDAO)<br>".$e;		
			}		
			return $res;
		}


		public static function userSawTheMessage($id_from,$id_to){
			$res = null;
			try {
				$query 	= "SELECT lido FROM `mensagem` WHERE id_from = ? AND id_to = ? AND lido = 1 ORDER BY id DESC LIMIT 1";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_from);
				$stmt->bindValue(2,$id_to);
				$stmt->execute();
				
				foreach ($stmt->fetchAll() as $value) {
					$res = $value->lido;
				}

			} catch (PDOException $e) {
				echo "ERRO AO FAZER FAZER CHECAGEM DE VISUALIZAÇÃO (MensagemDAO)<br>".$e;		
			}
			return $res;
		}



		public static function countMessagesBetweenMeAndOther($id_from,$id_to){
			$count = 0; 
			try {
				$query 	= "SELECT id FROM mensagem WHERE id_from = ? AND id_to = ?";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_from);
				$stmt->bindValue(2,$id_to);
				$stmt->execute();
				foreach ($stmt->fetchAll() as $val) { $count++; } 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER CONTAGEM DE MENSAGENS ENTRE MIM E OUTRO (MensagemDAO)<br>".$e;		
			}		
		
			return $count;
		}

		public static function listAllNotReaded(){
			$res = null; 
			try {
				$query 	= "SELECT * FROM mensagem WHERE lido = 0 ORDER BY id";					
				$stmt 	= Connection::prepare($query);
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER LISTAGEM (MensagemDAO)<br>".$e;		
			}		
		
			return $res;
		}


		public static function listByTimeStamp($time, $lastid){
			$res = null; 
			try {
				$query 	= "SELECT * FROM mensagem WHERE time >= ? $lastid AND lido = 0 ORDER BY id DESC";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$time);
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER LISTAGEM (MensagemDAO)<br>".$e;		
			}		
		
			return $res;
		}
		/* ++++++++++++++++++++++++SEARCH++++++++++++++++++++++++*/
		public static function verifyMsg($id_to){
			$res = null;
			try {
				$query  = "SELECT id_from FROM mensagem WHERE id_to = ? AND lido = ?  GROUP BY id_from ";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_to);
				$stmt->bindValue(2,0);
				
				$stmt->execute();
				$res = $stmt;
			} catch (PDOException $e) {
				echo "ERRO AO PESQUISAR PELO ID (PostagemDAO)";	
			}
			return $res;
		}

		/* ++++++++++++++++++++++++SEARCH++++++++++++++++++++++++*/

		/* ++++++++++++++++++++++++ UPDATE WHEN SEE THE MESSAGE ++++++++++++++++++++++++*/

		public static function updateStatus($id_from,$id_to){
			try {
				$query  = "UPDATE mensagem SET lido = ? WHERE id_to = ? AND id_from = ? OR id_to = ? AND id_from = ? ";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,1);
				$stmt->bindValue(2,$id_to);
				$stmt->bindValue(3,$id_from);
				$stmt->bindValue(4,$id_from);
				$stmt->bindValue(5,$id_to);						
				$stmt->execute();
			} catch (PDOException $e) {
				echo "ERRO UPDATE (PostagemDAO)";	
			}
		}

		/* ++++++++++++++++++++++++SEARCH++++++++++++++++++++++++*/

		
        
        
        
    
        /*  the function to erase all data of a user */
        public static function erase($id){
            try{
                $query = "DELETE FROM mensagem WHERE id_from = ? OR id_to = ?";
                $stmt  = Connection::prepare($query);
                $stmt->bindValue(1,$id);
                $stmt->bindValue(2,$id);
                $stmt->execute();
            }catch(PDOException $e){
                echo "ERROR AT ERASE... ALL :X (Mensagem)";
            }
        }
        /*  the function to erase all data of a user */
                
            
	}