<?php 
	require_once ('Connection.php');
	require_once ($root."/iforum/model/Comentario.php");
	class ComentarioDAO extends Connection{
		public static function comentar($c){
			try {
	            $query  = "INSERT INTO comentario (id_user,id_postagem,comentario,data_hora) VALUES(?,?,?,?)";
	            $stmt            = Connection::prepare($query);   
	            $stmt->bindValue(1, $c->getIdUser());
	            $stmt->bindValue(2, $c->getIdPost());
	            $stmt->bindValue(3, $c->getComentario());
	            $stmt->bindValue(4, $c->getDataHora());          
	            $stmt->execute();
	        } catch (PDOException $e) {
	            echo "ERRO AO Comentar (ComentarioDAO)\n".$e->getMessage();
	        }   
		}
		public static function listAll(){
			$res = null; 
			try {
				$query 	= "SELECT * FROM comentario ORDER BY id ASC";					
				$stmt 	= Connection::prepare($query);
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER LISTAGEM (ComentarioDAO)<br>".$e;		
			}		
			return $res;
		}

		public static function searchById($id){
			$comment = null; 
			try {
				$query 	= "SELECT * FROM comentario WHERE id = ? ORDER BY id ASC";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id);
				$stmt->execute();

				while($res = $stmt->fetch()){
					$comment = new Comentario($res->id_user,$res->id_postagem,$res->comentario,$res->data_hora,$res->visto);
					$comment->setId($res->id);
				}

			} catch (PDOException $e) {
				echo "ERRO AO FAZER LISTAGEM (ComentarioDAO)<br>".$e;		
			}		
			return $comment;
		}


		public static function commentsNotSeen($id_user){
			$res = null;
			try {
				$query = "SELECT comentario.id,comentario.id_user,comentario.data_hora,comentario.visto,comentario.id_postagem FROM comentario JOIN postagem ON comentario.id_postagem = postagem.id WHERE postagem.id_user = ? AND comentario.id_user != ? AND comentario.visto = 0 ORDER BY comentario.data_hora DESC";
				$stmt  = Connection::prepare($query);
				$stmt->bindValue(1,$id_user);
				$stmt->bindValue(2,$id_user);
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO VERIFICAR PESSOAS QUE COMENTARIO MINHA POSTAGEM... ";
			}
			return $res;
		}

		public static function delete($id){
			try {
				$query = "DELETE FROM comentario WHERE id = ?";
				$stmt = Connection::prepare($query);
				$stmt->bindValue(1, $id);
				$stmt->execute(); 
			} catch (PDOException $e) {
				echo "ERRO AO EXCLUIR COMENTÁRIO(ComentarioDAO)<br>".$e;		
			}

		}
        public static function deleteIfStared($id){
			try {
				$query = "UPDATE postagem SET melhor_resp = 0 WHERE melhor_resp = ?";
				$stmt = Connection::prepare($query);
				$stmt->bindValue(1, $id);
				$stmt->execute(); 
			} catch (PDOException $e) {
				echo "ERRO AO EXCLUIR 'IF STARED' COMENTÁRIO(ComentarioDAO)<br>".$e;		
			}

		}
        
		public static function searchByPostId($id_post){		
			$res = null; 
			try {
				$query 	= "SELECT * FROM comentario WHERE id_postagem = ? ORDER BY id ASC";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_post);
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER BUSCA (ComentarioDAO)<br>".$e;		
			}		
			return $res;
		}


		public static function deleteAllByIdPost($id){
			try {
	            $query = "DELETE FROM comentario WHERE id_postagem = ?";
	            $stmt = Connection::prepare($query);
	            $stmt->bindValue(1,$id);
	            $stmt->execute();
	        } catch (PDOException $e) {
	            echo "ERRO AO EXCLUIR OS COMENTARIOS REFERENTES AO POST TAL";
	        }
		}
        
        
        
        /* STAR COMMENTS  */
		public static function star($id_post,$id_comment){
			try {
	            $query = "UPDATE postagem SET melhor_resp = ? WHERE id = ? ";
	            $stmt = Connection::prepare($query);
	            $stmt->bindValue(1,$id_comment);
	            $stmt->bindValue(2,$id_post);
	            $stmt->execute(); 
	        } catch (PDOException $e) {
	            echo "ERRO AO ATUALIZAR 'STAR' (ComentarioDAO)";
	        }
		}        
        /* STAR COMMENTS  */        
        
                
        
        
        
        /*  the function to erase all data of a user */
        public static function erase($id){
            try{
                $query = "DELETE FROM comentario WHERE id_user = ?";
                $stmt  = Connection::prepare($query);
                $stmt->bindValue(1,$id);
                $stmt->execute();
            }catch(PDOException $e){
                echo "ERROR AT ERASE... ALL :X (Comentario)";
            }
        }
        /*  the function to erase all data of a user */
        
        
        

 }