<?php

	require_once ('Connection.php');
	$root =  $_SERVER['DOCUMENT_ROOT'];
	require_once ($root."/iforum/model/Usuario.php");
	require_once ($root."/iforum/controller/Utilitarios.php");


	class UsuarioDAO extends Connection{

		public static function save($u){
			try {
	            $query  = "INSERT INTO usuario (user,nome, sobrenome,genero, dia,
	            								 mes, ano,bio,link,email, cidade, telefone,
	            								 matricula, curso, serie, senha,status,img,noob)
											     VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	            $stmt            = Connection::prepare($query);
	            $stmt->bindValue(1, $u->getUserName());
	            $stmt->bindValue(2, $u->getNome());
	            $stmt->bindValue(3, $u->getSobrenome());
	            $stmt->bindValue(4, $u->getGenero());
	            $stmt->bindValue(5, $u->getDia());
	            $stmt->bindValue(6, $u->getMes());
	            $stmt->bindValue(7, $u->getAno());
	            $stmt->bindValue(8, $u->getBio());
	            $stmt->bindValue(9, $u->getLink());
	            $stmt->bindValue(10, $u->getEmail());
	            $stmt->bindValue(11, $u->getCidade());
	            $stmt->bindValue(12, $u->getTelefone());
	            $stmt->bindValue(13, $u->getMatricula());
	            $stmt->bindValue(14, $u->getCurso());
	            $stmt->bindValue(15, $u->getSerie());
	            $stmt->bindValue(16, $u->getSenha());
	            $stmt->bindValue(17, $u->getStatus());
	            $stmt->bindValue(18, $u->getImg());
	            $stmt->bindValue(19, $u->getNoob());

	        	$stmt->execute();
	        } catch (PDOException $e) {
	            echo "ERRO AO CADASTRAR USUÁRIO\n".$e->getMessage();
	        }
		}

		public static function authenticate($matricula, $senha){
			$user = null;
			try {
				$query 	= "SELECT * FROM usuario WHERE matricula = ? AND senha = ?";
				$stmt 	= Connection::prepare($query);

				$stmt->bindValue(1,$matricula);
				$stmt->bindValue(2,$senha);

				$stmt->execute();
				while($res = $stmt->fetch()){
					$user = new Usuario($res->user,$res->nome,$res->sobrenome,$res->genero,$res->dia,$res->mes,$res->ano,
										$res->bio, $res->link,$res->email,$res->cidade,$res->telefone,$res->matricula,$res->curso,
										$res->serie,$res->senha,$res->status,$res->img,$res->noob);

					$user->setId($res->id_u);
				}

			} catch (PDOException $e) {
				echo "ERRO AO FAZER LOGIN (UsuarioDAO)<br>".$e;
			}

			return $user;
		}


        public static function emailExists($email){
            $exist = false;
            try{
                $query = "SELECT email FROM usuario WHERE email = ?";
                $stmt  = Connection::prepare($query);

                $stmt->bindValue(1,$email);
                $stmt->execute();
                $exist = $stmt->RowCount();
            } catch(PDOException $e){
                echo "ERRO AO VERIFICAR SE O EMAIL EXISTE <br> $e";
            }
            return $exist;
        }

		public static function uploadImage($img,$id){
			try {
				$query = "UPDATE usuario SET img = ? WHERE id_u = ?";
				$stmt  = Connection::prepare($query);

				$stmt->bindValue(1,$img);
				$stmt->bindValue(2,$id);

				$stmt->execute();

			} catch (PDOException $e) {

			}
		}



		/* ++++++++++++++++++++++++ LIST ++++++++++++++++++++++++*/

		public static function listAll($except){
			$res = null;
			try {
	            $query = "SELECT * FROM usuario,amigo WHERE usuario.id_u != ? AND ((amigo.seguidor = ? AND usuario.id_u = amigo.seguido) OR (usuario.id_u = amigo.seguidor AND ? = amigo.seguido)) AND amigo.permissao = 2 ORDER BY nome ASC";
	            $stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$except);
				$stmt->bindValue(2,$except);
				$stmt->bindValue(3,$except);
				$stmt->execute();
				$res = $stmt;
			} catch (PDOException $e) {
				echo "ERRO AO LISTAR (UsuarioDAO)";
			}
			return $res;
		}

        
        public static function countFriends($id_user){
            $res = null;
            $count = 0;
			try {
	            $query = "SELECT * FROM usuario,amigo WHERE ((amigo.seguidor = ? AND usuario.id_u = amigo.seguido) OR (usuario.id_u = amigo.seguidor AND amigo.seguido = ?)) AND amigo.permissao = 2 ORDER BY nome ASC";
	            $stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_user);
				$stmt->bindValue(2,$id_user);
				$stmt->execute();
				$res = $stmt->fetchAll();
                foreach($res as $val){$count++;}
			} catch (PDOException $e) {
				echo "ERRO AO LISTAR (UsuarioDAO)";
			}
			return $count;
        }
        
		public function searchByMat($mat){
			$user = null;
			try {
				$query = "SELECT * FROM usuario WHERE matricula = ?";
				$stmt  = Connection::prepare($query);
                $stmt->bindValue(1,$mat);
                $stmt->execute();
                while($res = $stmt->fetch()){
                	$user = new Usuario($res->user,$res->nome,$res->sobrenome,$res->genero,$res->dia,$res->mes,$res->ano,
										$res->bio,$res->link,$res->email,$res->cidade,$res->telefone,$res->matricula,$res->curso,
										$res->serie,$res->senha,$res->status,$res->img,$res->noob);

					$user->setId($res->id_u);
                }
			} catch (PDOException $e) {

			}
			return $user;
		}
		public static function listByName($name,$id_user){
			$res = null;
			try {
				$query  = "SELECT * FROM usuario,amigo WHERE (  usuario.nome LIKE '$name%' OR usuario.sobrenome LIKE '$name%')  AND usuario.id_u != ?  AND ((amigo.seguidor = ? AND usuario.id_u = amigo.seguido) OR (usuario.id_u = amigo.seguidor AND ? = amigo.seguido)) AND amigo.permissao = 2  ORDER BY usuario.nome ASC ";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_user);
				$stmt->bindValue(2,$id_user);
				$stmt->bindValue(3,$id_user);
				$stmt->execute();
				$res = $stmt->fetchAll();
			} catch (PDOException $e) {
				echo "ERRO AO LISTAR PELO NOME (UsuarioDAO)";
			}
			return $res;
		}
		/* ++++++++++++++++++++++++ LIST ++++++++++++++++++++++++*/
		/* ++++++++++++++++++++++++SEARCH++++++++++++++++++++++++*/
		public static function searchById($id){
			$user = null;
			try {
				$query  = "SELECT * FROM usuario WHERE id_u = ?";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id);
				$stmt->execute();

				while($res = $stmt->fetch()){
					$user = new Usuario($res->user,$res->nome,$res->sobrenome,$res->genero,$res->dia,$res->mes,$res->ano,
										$res->bio,$res->link,$res->email,$res->cidade,$res->telefone,$res->matricula,$res->curso,
										$res->serie,$res->senha,$res->status,$res->img,$res->noob);

					$user->setId($res->id_u);
				}

			} catch (PDOException $e) {
				echo "ERRO AO PESQUISAR PELO ID (UsuarioDAO)";
			}
			return $user;
		}


		public static function searchByName($name){
			$user = null;
			try {
				$query  = "SELECT * FROM usuario WHERE usuario = ? LIMIT 1";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$name);
				$stmt->execute();

				while($res = $stmt->fetch()){
					$user = new Usuario($res->user,$res->nome,$res->sobrenome,$res->genero,$res->dia,$res->mes,$res->ano,
										$res->bio,$res->link,$res->email,$res->cidade,$res->telefone,$res->matricula,$res->curso,
										$res->serie,$res->senha,$res->status,$res->img,$res->noob);

					$user->setId($res->id_u);
				}

			} catch (PDOException $e) {
				echo "ERRO AO PESQUISAR PELO NOME (UsuarioDAO)";
			}
			return $user;
		}

		public static function searchByUserName($user_name){
			$user = null;
			try {
				$query  = "SELECT * FROM usuario WHERE user = ?  LIMIT 1";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,Utilitarios::removeAcentos($user_name));
				$stmt->execute();

				while($res = $stmt->fetch()){
					$user = new Usuario($res->user,$res->nome,$res->sobrenome,$res->genero,$res->dia,$res->mes,$res->ano,
										$res->bio,$res->link, $res->email,$res->cidade,$res->telefone,$res->matricula,$res->curso,
										$res->serie,$res->senha,$res->status,$res->img,$res->noob);

					$user->setId($res->id_u);
				}

			} catch (PDOException $e) {
				echo "ERRO AO PESQUISAR PELO NOME (UsuarioDAO)";
			}
			return $user;
		}

		/* ++++++++++++++++++++++++SEARCH++++++++++++++++++++++++*/


		public static function updateStatus($status,$matricula){
			try {
				$query 	= "UPDATE usuario SET status = ? WHERE matricula = ?";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$status);
				$stmt->bindValue(2,$matricula);

				$stmt->execute();
				$db = null;
			} catch (PDOException $e) {
				echo "ERRO AO ATUALIZAR O STATUS (UsuarioDAO)".$e;
			}
		}

		public static function setTimeLimit($id_user){
			try {
				date_default_timezone_set('America/Sao_Paulo');
				$hora_logado = date("Y-m-d H:i:s");
				$hora_que_expira = date("Y-m-d H:i:s", strtotime("+1 min"));
				$query 	= "UPDATE usuario SET horario_login = ?, limit_time = ? WHERE id_u = ?";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$hora_logado);
				$stmt->bindValue(2,$hora_que_expira);
				$stmt->bindValue(3,$id_user);

				$stmt->execute();
				$db = null;
			} catch (PDOException $e) {
				echo "ERRO AO SETAR TIME LIMIT (UsuarioDAO)".$e;
			}
		}
		public static function setLimitOnly($id_user,$hora_que_expira){
			try {

				$query 	= "UPDATE usuario SET  limit_time = ? WHERE id_u = ?";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$hora_que_expira);
				$stmt->bindValue(2,$id_user);

				$stmt->execute();
			} catch (PDOException $e) {
				echo "ERRO AO SETAR TIME LIMIT (UsuarioDAO)".$e;
			}
		}


		public static function verificarTempoLimite($id_user){
			$res = "";
			try {
				$query 	= "SELECT horario_login, limit_time FROM usuario WHERE id_u = ?";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_user);
				$stmt->execute();
				$res = $stmt->fetchObject();
			} catch (PDOException $e) {
				echo "ERRO AO VERIFICAR TIME LIMIT (UsuarioDAO)".$e;
			}
			return $res;
		}




		public static function updateNoob($id){
			try {
	            $query = "UPDATE usuario SET noob = 1 WHERE id_u = ?";
	            $stmt  = Connection::prepare($query);
	            $stmt->bindValue(1,$id);
	            $stmt->execute();
            } catch (PDOException $e) {
                echo "ERRO AO NOOBAR";
            }
		}





		public static function view_friend_notification($id){
			try {
				$query = "UPDATE amigo SET visto = 1 WHERE id = ?";
				$stmt  = Connection::prepare($query);
				$stmt->bindValue(1,$id);
				$stmt->execute();
			} catch (PDOException $e) {
				echo "ERRO AO 'VISUALIZAR' SOLICITAÇÃO DE AMIZADE";
			}

		}
	
		public static function follow($follower,$followed){
			try {
	            $query = "INSERT INTO amigo (seguidor,seguido,permissao) VALUES(?,?,?)";
	            $stmt  = Connection::prepare($query);
	            $stmt->bindValue(1,$follower);
	            $stmt->bindValue(2,$followed);
	            $stmt->bindValue(3,1);

	            $stmt->execute();
            } catch (PDOException $e) {
                echo "ERRO AO SEGUIR (UsuarioDAO)";
            }
		}

		public static function unfollow($follower,$followed){
			try {
	            $query = "DELETE FROM amigo WHERE seguidor = ? AND seguido = ? OR seguidor = ? AND seguido = ?";
	            $stmt  = Connection::prepare($query);
	            $stmt->bindValue(1,$follower);
	            $stmt->bindValue(2,$followed);
	            $stmt->bindValue(3,$followed);
	            $stmt->bindValue(4,$follower);
	            $stmt->execute();
            } catch (PDOException $e) {
                echo "ERRO AO DEIXAR DE SEGUIR (UsuarioDAO)";
            }
		}


		public static function isFollowing($follower,$followed){
			$res = 0;
			try {
				$query = "SELECT seguidor,permissao FROM amigo WHERE seguidor = ? AND seguido = ? OR seguidor = ? AND seguido = ?";
				$stmt = Connection::prepare($query);
				$stmt->bindValue(1,$follower);
				$stmt->bindValue(2,$followed);
				$stmt->bindValue(3,$followed);
				$stmt->bindValue(4,$follower);
				$stmt->execute();
				$res = $stmt->fetchAll();
			} catch (PDOException $e) {
				echo "ERRO AO VERIFICAR 'FOLLOW' (UsuarioDAO)";
			}
			return $res;
		}

         public static function friendRequest($id_follower){
            $res = "";
            try{
                $query = "SELECT * FROM amigo WHERE seguido = ? AND permissao = 1  ORDER BY id DESC" ;
                $stmt  = Connection::prepare($query);
                $stmt->bindValue(1,$id_follower);
                $stmt->execute();
                $res = $stmt->fetchAll();
            }catch(PDOException $e){
                echo "ERRO AO CHECAR NOVA SOLICITAÇÃO... (UsuarioDAO) ";
            }
            return $res;
        }

        public static function acceptFollow($follower,$followed){
			try {
	            $query = "UPDATE amigo SET permissao = 2 WHERE seguidor = ? AND seguido = ?";
	            $stmt  = Connection::prepare($query);
	            $stmt->bindValue(1,$follower);
	            $stmt->bindValue(2,$followed);
	            $stmt->execute();
            } catch (PDOException $e) {
                echo "ERRO AO REJEITAR O PEDIDO -> SEGUIR (Usuario)";
            }
		}
        public static function rejectFollow($follower,$followed){
			try {
	            $query = "DELETE FROM amigo WHERE seguidor = ? AND seguido = ?";
	            $stmt  = Connection::prepare($query);
	            $stmt->bindValue(1,$follower);
	            $stmt->bindValue(2,$followed);
	            $stmt->execute();
            } catch (PDOException $e) {
                echo "ERRO AO REJEITAR O PEDIDO -> SEGUIR (Usuario)";
            }
		}










        /*  the function to erase all data of a user */
        public static function erase($id){
            try{
                $query = "DELETE FROM usuario WHERE id_u = ?";
                $stmt  = Connection::prepare($query);
                $stmt->bindValue(1,$id);
                $stmt->execute();
            }catch(PDOException $e){
                echo "ERROR AT ERASE... ALL :X ";
            }
        }

        /*  the function to erase all data of a user */


















	}
