<?php 
	require_once ('Connection.php');
	$root =  $_SERVER['DOCUMENT_ROOT'];
	require_once ($root."/iforum/model/Postagem.php");
	class PostagemDAO extends Connection{
		public static function publicar($p){
			try {
	            $query  = "INSERT INTO postagem (id_user,duvida,categoria,data_hora) VALUES(?,?,?,?)";
	            $stmt            = Connection::prepare($query);
	            $stmt->bindValue(1, $p->getId_user());
	            $stmt->bindValue(2, $p->getDuvida());
	            $stmt->bindValue(3, $p->getCategoria());
	            $stmt->bindValue(4, $p->getDataHora());
	            $stmt->execute();  	
	        } catch (PDOException $e) {
	            echo "ERRO AO PUBLICAR (PostagemDAO)\n".$e->getMessage();
	        }   
		}
		public static function listAll($id_user){
			$res = null; 
			try {
				$query 	= "SELECT * FROM postagem WHERE id_user = ? ORDER BY id DESC";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_user); 
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER LISTAGEM (PostagemDAO)<br>".$e;		
			}		
			return $res;
		}
		
		public static function listWithLimit($id_user,$limit){
			$res = null; 
			try {
				$query 	= "SELECT * FROM postagem WHERE id_user = ? ORDER BY id DESC LIMIT $limit";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_user); 

				
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO FAZER LISTAGEM COM LIMITE (PostagemDAO)<br>".$e;		
			}		
		
			return $res;
		}
		

		public static function countAll($id_user){
			$count = 0;
			try {
				$query 	= "SELECT count(*) FROM postagem WHERE id_user = ?";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_user); 
				$stmt->execute();
				$res = $stmt->fetch(); 
				foreach ($res as $key => $value) {$count = $value;}
			} catch (PDOException $e) {
				echo "ERRO AO FAZER CONTAGEM (PostagemDAO)<br>".$e;		
			}		
			return $count;
		}


		
		public static function listByCategory($materia){
			$res = null;
			try {
				$query   = "SELECT * FROM postagem WHERE categoria = ? ORDER BY id DESC";
				$stmt    = Connection::prepare($query);
				$stmt->bindValue(1,$materia);
				$stmt->execute();
				$res 	 = $stmt->fetchAll();
			}catch (PDOException $e) {
				
			}
			return $res; 
		} 
		/* ++++++++++++++++++++++++SEARCH++++++++++++++++++++++++*/
		public static function searchById($id){
			$forum = null;
			try {
				$query  = "SELECT * FROM postagem WHERE id = ?";
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id);
				$stmt->execute();
				
				while($res = $stmt->fetch()){
					$forum = new Postagem($res->id_user,$res->duvida,$res->categoria,$res->data_hora);
					$forum->setId($res->id);
				}

			} catch (PDOException $e) {
				echo "ERRO AO PESQUISAR PELO ID (PostagemDAO)";	
			}
			return $forum;
		}
		/* ++++++++++++++++++++++++SEARCH++++++++++++++++++++++++*/

		/* ++++++++++++++++++++++++ COUNT FORUMS ++++++++++++++++++++++++*/
		public static function countForumsByCategory($category){
			$res 	= null;
			$count  = 0;
			try {
				$query   = "SELECT * FROM postagem WHERE categoria = ? ORDER BY id DESC";
				$stmt    = Connection::prepare($query);
				$stmt->bindValue(1,$category);
				$stmt->execute();
				$res 	 = $stmt->fetchAll();
				foreach ($res as $key) { $count++; }
			}catch (PDOException $e) {
			}
			return $count; 
		} 
		/* ++++++++++++++++++++++++ COUNT FORUMS ++++++++++++++++++++++++*/


		/* ++++++++++++++++++++++++ DELETE POST ++++++++++++++++++++++++*/
		public static function delete($id_post){
        	$check = 0;
        	try {
	        	$query 	= "DELETE FROM postagem WHERE id = ?";
	            $stmt 	= Connection::prepare($query);
	            $stmt->bindValue(1,$id_post);
	            $check = $stmt->execute();	

        	} catch (PDOException $e) {
        		echo "ERRO AO EXCLUIR POSTAGEM!! (Postagem)";
        	}
        	return $check;
		}

		/* ++++++++++++++++++++++++ DELETE POST ++++++++++++++++++++++++*/


		
        
        public static function like($id_post,$id_user,$date){
			$res = "";
			try {
	            $verify = "SELECT id FROM curtida WHERE id_user = ? AND id_postagem = ?";
	            $stmt_verify = Connection::prepare($verify);
	            $stmt_verify->bindValue(1,$id_user);
	            $stmt_verify->bindValue(2,$id_post);
	            $stmt_verify->execute();
	            $count = $stmt_verify->fetchAll();
	            $c = 0;
	            foreach ($count as $key => $value) {$c++;}
	            if($c == 0){
		            $query  = "INSERT INTO curtida (id_postagem,id_user,data) VALUES(?,?,?)";
		            $stmt            = Connection::prepare($query);
		            $stmt->bindValue(1, $id_post);
		            $stmt->bindValue(2, $id_user);
		            $stmt->bindValue(3, $date);
		            $stmt->execute();
	            	$res = "liked";
	            }else{
					$query  = "DELETE FROM curtida WHERE id_postagem = ? AND id_user = ?";
		            $stmt            = Connection::prepare($query);
		            $stmt->bindValue(1, $id_post);
		            $stmt->bindValue(2, $id_user);
		            $stmt->execute();	            	
	            	$res = "unliked";
	            }  	
	        } catch (PDOException $e) {
	            echo "ERRO AO CURTIR(PostagemDAO)\n".$e->getMessage();
	        }
	        return $res;
		}
		public static function countLikes($id_post){
			$count = 0;
			try {
				$query 	= "SELECT count(id) FROM curtida WHERE  id_postagem = ?";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_post); 
				$stmt->execute();
				$res = $stmt->fetch(); 
				foreach ($res as $key => $value) {$count = $value;}
			} catch (PDOException $e) {
				echo "ERRO AO FAZER CONTAGEM DE LIKES (PostagemDAO)<br>".$e;		
			}		
			return $count;
		}
		public static function wasLikedByMe($id_post,$id_user){
			$count = 0;
			try {
				$query 	= "SELECT count(id_user) FROM curtida WHERE id_user = ? AND id_postagem = ?";					
				$stmt 	= Connection::prepare($query);
				$stmt->bindValue(1,$id_user); 
				$stmt->bindValue(2,$id_post); 
				$stmt->execute();
				$res = $stmt->fetch(); 
				foreach ($res as $key => $value) {$count = $value;}
			} catch (PDOException $e) {
				echo "ERRO AO VERIFICAR SE VOCÊ GOSTOU (PostagemDAO)<br>".$e;		
			}		
			return $count;
		}

		public static function peopleLikedMyPost($id_post){
			$res = null;
			try {
				$query = "SELECT * FROM curtida JOIN usuario ON curtida.id_user = usuario.id_u WHERE curtida.id_postagem = ? ORDER BY curtida.data DESC";
				$stmt  = Connection::prepare($query);
				$stmt->bindValue(1,$id_post);
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO VERIFICAR PESSOAS QUE CURTIRAM SUA POSTAGEM... ";
			}
			return $res;
		}

		public static function seeRequests($table,$id){
			try {
	            $query = "UPDATE $table SET visto = 1 WHERE id = ?";
	            $stmt = Connection::prepare($query);
	            $stmt->bindValue(1,$id);
	            $stmt->execute(); 
	        } catch (PDOException $e) {
	            echo "ERRO AO 'VER' REQUEST(PostagemDAO)";
	        }
		}       

		public static function likesNotSeen($id_user){
			$res = null;
			try {
				$query = "SELECT curtida.id,curtida.id_user,curtida.data,curtida.visto,curtida.id_postagem FROM curtida JOIN postagem ON curtida.id_postagem = postagem.id WHERE postagem.id_user = ?  AND curtida.id_user != ? AND curtida.visto = 0 ORDER BY curtida.data DESC";
				$stmt  = Connection::prepare($query);
				$stmt->bindValue(1,$id_user);
				$stmt->bindValue(2,$id_user);
				$stmt->execute();
				$res = $stmt->fetchAll(); 
			} catch (PDOException $e) {
				echo "ERRO AO VERIFICAR PESSOAS QUE CURTIRAM SUA POSTAGEM... \n $e";
			}
			return $res;
		}
        

        /*  the function to erase all data of a user */
        public static function erase($id){
            try{
                $query = "DELETE FROM postagem WHERE id_user = ?";
                $stmt  = Connection::prepare($query);
                $stmt->bindValue(1,$id);
                $stmt->execute();
            }catch(PDOException $e){
                echo "ERROR AT ERASE... ALL :X (Postagem)";
            }
        }
        /*  the function to erase all data of a user */
        
        
		

	}