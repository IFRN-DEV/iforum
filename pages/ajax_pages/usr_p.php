<?php 
	session_start(); 
	$root = $_SERVER['DOCUMENT_ROOT'];
	require_once $root.'/iforum/DAO/UsuarioDAO.php';  
    require_once $root.'/iforum/DAO/PostagemDAO.php';
    require_once $root.'/iforum/model/Postagem.php';
    require_once $root.'/iforum/model/Comentario.php';
    require_once $root.'/iforum/DAO/ComentarioDAO.php';
    require_once $root.'/iforum/controller/Utilitarios.php';
	
	if(isset($_POST['action'])){
		$action  = $_POST['action']; 
    	$RETORNO = array();	
	} 
    if($action == "post"){
    	$id_user    = $_POST["id_user"];
        $duvida     = strip_tags($_POST["duvida"]);
        $categoria  = $_POST["categoria"];
        //  +  ==  (#*@&?_-_/\'maissiam\'/_-_?&@*#)
        date_default_timezone_set('America/Sao_Paulo');
        $date = date("d m Y H:i:s");
        $date_array = explode(" ", $date);
        $dia    = $date_array[0];
        $mes    = $date_array[1];
        $ano    = $date_array[2];
        $hora   = $date_array[3];
        $mes = Utilitarios::getMonthName($mes);

        $data_hora = $dia." de ".$mes." de ".$ano." às ".$hora;
        PostagemDAO::publicar(new Postagem($id_user, $duvida, $categoria, $data_hora));
    }
    else if($action == "list"){
    	
        $id_user    = $_SESSION['id_user_posts'];    
		$limit      = $_POST['limit'];
        $posts      = PostagemDAO::listWithLimit($id_user,$limit);
        $count      = PostagemDAO::countAll($id_user);  
        
        $remaining = $count - $limit;

        if($posts != null){
			foreach ($posts as $key => $value) {
                date_default_timezone_set('America/Sao_Paulo');
                $data_hora = 0;
                $dia_atual  =  date('d');
                $hora_atual =  date('H');
                $min_atual  =  date('i');
                $seg_atual  =  date('s');
                $data_array =  explode(" " , $value->data_hora);
                $mes_post   =  $data_array[2];
                $dia_post   =  $data_array[0]; 
                $hora_array =  explode(":", $data_array[6]);
                $hora_post  = $hora_array[0];
                $min_post   = $hora_array[1];
                $seg_post   = $hora_array[2];
                if($dia_atual > $dia_post){
                    $d = $dia_atual-$dia_post;
                    if($d == 1){
                        $data_hora = "Ontem às ".$hora_post.":".$min_post;
                    }else{
                        $data_hora = $dia_post." de ".$mes_post;    
                    }
                    
                }else if($dia_atual == $dia_post){
                    
                    $h = $hora_atual-$hora_post;
                    if($h == 0){
                         $m = $min_atual-$min_post;
                         if($m == 0){
                            $data_hora = "agora mesmo";
                         }else{
                            $data_hora = $min_atual-$min_post." min";    
                            if(($min_atual-$min_post) > 1){ $data_hora = $min_atual-$min_post." mins"; }
                         }
                    }else{
                        $data_hora = $hora_atual-$hora_post." h";
                        if(($hora_atual-$hora_post) > 1){ $data_hora = $hora_atual-$hora_post." hrs"; }
                    }
                }else{
                    $data_hora = $dia_post." de ".$mes_post." às ".$hora_post.":".$min_post;  
                }
				$user 	= UsuarioDAO::searchById($value->id_user);
				//MELHOR RESPOSTA
                $melhor_resp = '';
                if($value->melhor_resp != '') $melhor_resp = $value->melhor_resp; else $melhor_resp = 0;
                //COUNT LIKES
                $countLikes = PostagemDAO::countLikes($value->id);
                $liked      = PostagemDAO::wasLikedByMe($value->id,$_SESSION['id']);

                if($id_user == $_SESSION['id']){
                    $post = " <div class='post box $value->categoria' id='$value->id' likes='$countLikes' likedbyme='$liked' stared='$melhor_resp'><div class='row'><div class='col-xs-9 col-sm-6 col-md-6 col-lg-6'><div class='row'><div class='col-md-4 col-lg-3' ><img src=".$user->getImg()." width='60px' height='60px' style='margin-top:10px;' class='img-circle'></div><div class='col-md-8 col-lg-9'><br><un><b>".$user->getNome()." ". $user->getSobrenome()." </b></un><br><small style='cursor:pointer; font-size:11px' data-toggle='tooltip' data-placement='bottom'  title='$value->data_hora'> ".$data_hora." </small></div></div></div><div class='col-xs-3 col-sm-6 col-md-6 col-lg-6'><!-- Large button group --><div style='margin-top:15px' class='navbar pull-right'><!-- <button class='dropdown-toggle' ><span class='glyphicon glyphicon-chevron-down icon'></span></button>--><button data-toggle='dropdown' aria-expanded='false' style='outline:none'><span class='glyphicon glyphicon-chevron-down icon-post' style='font-size:11px;'></span></button><ul class='dropdown-menu' role='menu'><li id_post=".$value->id." class='edit-post'><a><i class='mdi mdi-pencil icon' style='color:#555;font-size:17px'></i> Editar</a></li><li id_post=".$value->id." class='delete-post'><a><i style='color:#555;font-size:17px' class='mdi mdi-delete icon'></i> Excluir</a></li></ul></div></div></div>  <br><div class='row access'><div class='col-md-12'> <div class='edit-area-post'> <p style='font: 300 17px Roboto;color:#5F5F5F;text-align:justify '>".$value->duvida."</p></div><div class='row'><div class='col-md-12'><hr><span class='do_like mdi mdi-thumb-up'></span><count style='font:12.2px Roboto;color:#666; cursor:pointer' class='count_likes '> 0</count> <span class='comment-icon btn-resp mdi mdi-comment'></span><count class='count-comentarios' style='font:12.2px Roboto;color:#666;cursor:pointer'> 0</count></div></div><div class='row comentarios'><br><div class='col-md-12'><div class='row'><div class='col-md-12'> <div class='row '><div  class='cx-comments col-md-12'> <div class='comments'><!-- respostas aqui --></div><br><br> </div>        </div><div class='row'><div class='col-md-11 col-lg-11 col-sm-11 col-xs-11'><textarea class='cx-comentar' placeholder='Deixe uma resposta...' data-autoresize></textarea> </div><div class='col-xs-1 col-sm-1  col-md-1 col-lg-1'><span class='send_comment pull-right'><i style='font-size:22px; background:#2FAF70;color:#FFF; padding:5px;margin-right:5px' class='pull-right img-circle mdi mdi-send'></i>     </span><input id='id_post' type='hidden' value='$value->id'></div></div><!-- COMENTAR --><br><center><button class='btn-default hide_comments' ><span class='glyphicon glyphicon-chevron-up' style='font-size: 12px;'></span></button></center></div></div></div></div></div></div></div>";
                }else{
                    $post = "<div class='post box $value->categoria' id='$value->id' likes='$countLikes' likedbyme='$liked' stared='$melhor_resp'><div class='row'><div class='col-xs-9 col-sm-6 col-md-6 col-lg-6'><div class='row'><div class='col-md-4 col-lg-3' ><img src=".$user->getImg()." width='60px' height='60px' style='margin-top:10px;' class='img-circle'></div><div class='col-md-8 col-lg-9'><br><un><b>".$user->getNome()." ". $user->getSobrenome()." </b></un><br><small style='cursor:pointer; font-size:11px' data-toggle='tooltip' data-placement='bottom'  title='$value->data_hora'> ".$data_hora." </small></div></div></div><div class='col-xs-3 col-sm-6 col-md-6 col-lg-6'><!-- Large button group --></div></div><br><div class='row access'><div class='col-md-12'> <div class='edit-area-post'> <p style='font: 300 17px Roboto;color:#5F5F5F; text-align: justify'>".$value->duvida."</p></div><div class='row'><div class='col-md-12'><hr><span class='do_like mdi mdi-thumb-up'></span><count style='font:12.2px Roboto; color:#666;cursor:pointer' class='count_likes'> 0</count><span class='comment-icon btn-resp mdi mdi-comment'></span><count class='count-comentarios' style='font:12.2px Roboto; color:#666; cursor:pointer'> 0</count></div></div><div class='row comentarios'><br><div class='col-md-12'><div class='row'><div class='col-md-12'> <div class='row '><div  class='cx-comments col-md-12'> <div class='comments'><!-- respostas aqui --></div><br><br> </div>        </div><div class='row'><div class='col-md-11 col-lg-11 col-sm-11 col-xs-11'><textarea class='cx-comentar' placeholder='Deixe uma resposta...' data-autoresize></textarea> </div><div class='col-xs-1 col-sm-1  col-md-1 col-lg-1'><span class='send_comment pull-right'><i style='font-size:22px; background:#2FAF70;color:#FFF; padding:5px;margin-right:5px' class='pull-right img-circle mdi mdi-send'></i>     </span><input id='id_post' type='hidden' value='$value->id'></div></div><!-- COMENTAR --><br><center><button class='btn-default hide_comments' ><span class='glyphicon glyphicon-chevron-up' style='font-size: 12px;'></span></button></center></div></div></div></div></div></div></div>";
                } 
            $RETORNO[] = array(
                'post'          => $post,
                'id'            => $value->id,
                'count'         => $count,
                'remaining'     => $remaining,
                'content'         => $value->duvida
                );
			}
		}else{
			if($id_user == $_SESSION['id']){
                $RETORNO[] = array(
                               'post' => " <center> <i style='font-size:90px; color:#DADADA;text-shadow:0 -1px #CCC;' class='mdi mdi-forum'></i>     
                               <br><br>   <small><h4 style='color:#BBB; text-shadow:0 1px #FAFAFA;'>Nenhuma dúvida ainda...</h4></small></center>",
                               'id'   => 0,
                               'count' => $count,
                               'remaining'     => $remaining
                            );
            }else{
                $u = UsuarioDAO::searchById($id_user);
                $RETORNO[] = array(
                                'post' =>  " <center> <i style='font-size:90px; color:#DADADA;text-shadow:0 -1px #CCC;' class='mdi mdi-forum'></i>     
                               <br><br>   <small><h4 style='color:#BBB; text-shadow:0 1px #FAFAFA;'>".$u->getNome()." ainda não tem dúvidas...</h4></small></center>",
                                'id'   =>  0,
                                'count' => $count,
                                'remaining' => $remaining   
                            );
            }
		
        }
        die(json_encode($RETORNO));
    }else if($action == 'listforuns'){
        $materia    = $_POST['materia'];
        $foruns     = PostagemDAO::listByCategory($materia);
        $resp = array();
        if(!empty($foruns)){
            foreach ($foruns as $key => $forum) {
                $u = UsuarioDAO::searchById($forum->id_user);
                //Check best answer
                if($forum->melhor_resp != 0){
                    $c = ComentarioDAO::searchById($forum->melhor_resp);
                    foreach ($c as $key => $val) {
                        $melhor_resp =  "<i style='font-size:20px;' class='mdi mdi-trophy-award stared'></i>".substr($val->comentario, 0,20)."..."; 
                    }
                }else{
                    $melhor_resp = "<i>Sem melhor resposta</i>";
                }
                //Check best answer

                // count foruns 
                $com = ComentarioDAO::searchByPostId($forum->id);
                
                $count       = 0;
                $count_resps = "0 respostas";
                foreach($com as $key) {  
                    $count++;
                    if($count > 1){
                        $count_resps = $count." respostas";
                    }else if($count == 1){
                        $count_resps = $count." resposta";
                    }
                } 
                // count foruns 

                $f = "<div class='forum $materia box' id='$materia/$forum->id'>
                    <div class='row'> 
                        <div class='col-lg-2 col-md-2 col-sm-2 col-sx-2'>
                            <center><img  src='".$u->getImg()."' width='90' height='90' style='margin:3px 0 0 0;' class='img-circle'></center>
                        </div>
                        <div class='col-lg-10 col-md-10 col-sm-10 col-sx-10' style='padding:3px 0 0 0;'>
                            <h4>
                                <a href='/iforum/".$u->getUserName()."'><b>".$u->getNome()." ".$u->getSobrenome()."</b></a>:
                                ".substr($forum->duvida, 0,25)."...
                            </h4>    
                            <p title='Melhor resposta...'>$melhor_resp</p>      
                            <small style='font-size:11.7px'> ".$count_resps." - $forum->data_hora  </small>
                </div>";

                $resp[] = array('id' => $forum->id,'forum' => $f);

            }//end loop
        }else{
            $resp[] = array('id' => 0, 'forum' => '<center> <img src="/iforum/res/imgs/ding.png"><br><br><h4><small> Nada por aqui ainda...</small></h4> </center>');
        }
        die(json_encode($resp));
                              
    }
    else if($action == "del"){ 
        $id = $_POST['id'];
        ComentarioDAO::deleteAllByIdPost($id);
        PostagemDAO::delete($id);
    }
    else if($action == 'update-post'){
        $id_post    = $_POST['id_edited'];
        $val        = $_POST['val'];
        try {
            $query = "UPDATE postagem SET duvida = ? WHERE id = ?";
            $stmt  = Connection::prepare($query);
            $stmt->bindValue(1,$val);
            $stmt->bindValue(2,$id_post);
            $stmt->execute();
               
        } catch (PDOException $e) {
            echo "ERRO AO ATUALIZAR POSTAGEM (PostagemDAO)";
        }
    }

    else if($action == 'update-comment'){
        $id_comment    = $_POST['id_edited'];
        $val        = $_POST['val'];
        try {
            $query = "UPDATE comentario SET comentario = ? WHERE id = ?";
            $stmt  = Connection::prepare($query);
            $stmt->bindValue(1,$val);
            $stmt->bindValue(2,$id_comment);
            $stmt->execute();
               
        } catch (PDOException $e) {
            echo "ERRO AO ATUALIZAR COMENTÁRIO (PostagemDAO)";
        }
    }

    else if($action == "list_c"){
        $resp   = array();
        $posts  = $_POST['list_posts'];
        $count  = 0;     
        foreach ($posts as $key => $value) {
            $id_post  =  $value;
            $comments =  ComentarioDAO::searchByPostId($value);
            if(!empty($comments)){
                foreach ($comments as $key => $value) {
                    $count++;
                    $id_user_comment = $value->id_user;
                    $u      = UsuarioDAO::searchById($id_user_comment);
                    $notMine = ""; 
                    if($id_user_comment == $_SESSION['id']){ 
                        $cm = " <div class='navbar pull-right'>
                                    <button data-toggle='dropdown' aria-expanded='false' style='outline:none'>
                                         <span class='glyphicon glyphicon-chevron-down icon-comment' style='margin-top: 20px;'></span>
                                    </button>
                                    <ul class='dropdown-menu' role='menu'>
                                        <li id_comment='$value->id' class='edit-comment'>
                                            <a><span style='font-size:16px' class='mdi mdi-pencil icon'></span>  Editar</a>
                                        </li>
                                        <li id_comment='$value->id' class='delete-comment'>
                                            <a><span style='font-size:16px' class='mdi mdi-delete icon'></span>  Excluir</a>
                                        </li>
                                    </ul>
                                </div>    
                               ";     
                                                           
                    }else{
                        $notMine = "notMine";
                        $cm = "<div class='navbar pull-right'></div>";    
                    } 
                    
                                 
                    /* COISAS DA DATA */
                    date_default_timezone_set('America/Sao_Paulo');
                        $data_hora = 0;
                        $dia_atual  =  date('d');
                        $hora_atual =  date('H');
                        $min_atual  =  date('i');
                        $seg_atual  =  date('s');
                        $data_array =  explode(" " , $value->data_hora);
                        $mes_post   =  $data_array[2];
                        $dia_post   =  $data_array[0]; 
                        $hora_array =  explode(":", $data_array[6]);
                        $hora_post  = $hora_array[0];
                        $min_post   = $hora_array[1];
                        $seg_post   = $hora_array[2];
                        if($dia_atual > $dia_post){
                            $d = $dia_atual-$dia_post;
                            if($d == 1){
                                $data_hora = "Ontem às ".$hora_post.":".$min_post;
                            }else{
                                $data_hora = $dia_post." de ".$mes_post." às ".$hora_post.":".$min_post;    
                            }
                            
                        }else if($dia_atual == $dia_post){
                            
                            $h = $hora_atual-$hora_post;
                            if($h == 0){
                                 $m = $min_atual-$min_post;
                                 if($m == 0){
                                    $data_hora = "agora mesmo";
                                 }else{
                                    $data_hora = $min_atual-$min_post."m";    
                                 }
                            }else{
                                $data_hora = $hora_atual-$hora_post."h";
                            }
                        }else{
                            $data_hora = $dia_post." de ".$mes_post." às ".$hora_post.":".$min_post;  
                        }
                    /* COISAS DA DATA */
                    
                    if($_SESSION['id_user_posts'] == $_SESSION['id'] && $_SESSION['id_user_posts'] != $u->getId()){
                        $star = "<br><span id='$value->id' id_post='$id_post' title='Melhor resposta' class='star-this-comment mdi mdi-star icon pull-left'></span>"; 
                    } else{$star = "";}


                    $comm = "<div id=".$value->id." class='row comment $notMine' style='cursor:pointer'>
                        <div class='col-md-1'>
                            <div>
                                <img class='img-circle' width='40px' height='40px' src=".$u->getImg().">
                            </div>
                        </div>
                        <div class='col-md-11'> 
                            <div>
                                $cm
                                <a href='/iforum/".$u->getUserName()."' style='color:#555'><b>".$u->getNome()." ".$u->getSobrenome()."</b></a>
                                ·<small style='cursor:pointer' title='$value->data_hora'>  $data_hora </small>
                                $star<br>   
                                <div class='edit-area-comment'>
                                    <p style='font: 13px Roboto;'>$value->comentario</p><br>                                    
                                </div>
                            </div>
                        </div>   
                    </div>
                    <br>";
                    
                    $resp[] = array(   
                        'comment' => $comm,
                        'id_post' => $value->id_postagem,
                    );
                }                
            }
        }
        
        die(json_encode($resp));
          
    }
    else if($action == 'like'){
        date_default_timezone_set('America/Sao_Paulo');
        $date = date("Y-m-d h:i:s");                    
        $res  =  PostagemDAO::like($_POST['id_post'],$_POST['id_user'],$date);
        die($res);
    }
    else if($action == 'peopleLikedMyPost'){
        $id_post = $_POST['id'];
        $people  = PostagemDAO::peopleLikedMyPost($id_post);
        $res = array();
        foreach ($people as $key => $value) {
            $check = UsuarioDAO::isFollowing($_SESSION['id'],$value->id_u);$c = 0;foreach ($check as $k) {$c++;}
            if($c == 1){
                $person[] = " <br><div class='$c row'><div class='col-xs-1 col-sm-1 col-md-1 col-lg-1'><img src='$value->img' height='55' width='55' alt='$value->nome' class='img-circle'></div><div class='col-xs-11 col-sm-11 col-md-1 col-lg-11'><div  style='margin-left:20px; margin-top:15px'> <h5 style='cursor:pointer' username='$value->user'>  <a style='color:#2FB069' href='/iforum/$value->user' > $value->nome $value->sobrenome </a> <div class='pull-right'><span class='mdi mdi-check-all' style='font-size:20px; color:#239966'></span><small>Amigos</small></div><br><small>Informática</small></h5></div></div></div><br>";
            }else{
                $person[] = " <br><div class='$c row'><div class='col-xs-1 col-sm-1 col-md-1 col-lg-1'><img src='$value->img' height='55' width='55' alt='$value->nome' class='img-circle'></div><div class='col-xs-11 col-sm-11 col-md-1 col-lg-11'><div  style='margin-left:20px; margin-top:15px'><h5 style='cursor:pointer' username='$value->user'> <a style='color:#2FB069' href='/iforum/$value->user' > $value->nome $value->sobrenome </a>  <br><small>Informática</small></h5></div></div></div><br>";
            }    
            $res['people']['person'] = $person;    
        }
        die(json_encode($res));
    }
    

    else if($action == 'seeRequests'){
        if(isset($_POST['notificationType'])){ $notificationType = $_POST['notificationType'];}
        if(isset($_POST['likes'])  ||  isset($_POST['comments'])){
            foreach ($_POST['likes'] as $id) {
                PostagemDAO::seeRequests("curtida",$id);
            }
            foreach ($_POST['comments'] as $id) {
                PostagemDAO::seeRequests("comentario",$id);
            }
        }
        else{
            if($notificationType == "curtida"){
                PostagemDAO::seeRequests($notificationType,$_POST['id']);
            }else{
                PostagemDAO::seeRequests($notificationType,$_POST['id']);
            }
        }
    }



    else if($action == 'comment'){
        $comment = $_POST['comment'];
        $id_post = $_POST['id_post'];
        $id_user = $_SESSION['id'];

        date_default_timezone_set('America/Sao_Paulo');
        $date = date("d m Y H:i:s");
        $date_array = explode(" ", $date);
        $dia    = $date_array[0];
        $mes    = $date_array[1];
        $ano    = $date_array[2];
        $hora   = $date_array[3];
        $mes = Utilitarios::getMonthName($mes);
        $data_hora = $dia." de ".$mes." de ".$ano." às ".$hora;
        $com = new Comentario($id_user,$id_post,$comment,$data_hora);
        ComentarioDAO::comentar($com);}
    else if($action == 'del_c'){
        $id = $_POST['id_c'];
        ComentarioDAO::deleteIfStared($id);
        ComentarioDAO::delete($id);
    }
    /* STAR */
    else if($action == 'star'){
        $id_comment = $_POST['id_comment'];
        $id_post = $_POST['id_post'];
        ComentarioDAO::star($id_post,$id_comment);
    }

        