    <?php
	$root = $_SERVER['DOCUMENT_ROOT'];
    require_once $root.'/iforum/model/Usuario.php';
    require_once $root.'/iforum/DAO/UsuarioDAO.php';
    require_once $root.'/iforum/model/Mensagem.php';
    require_once $root.'/iforum/DAO/MensagemDAO.php';
    require_once $root.'/iforum/DAO/PostagemDAO.php';
    require_once $root.'/iforum/DAO/ComentarioDAO.php';
    require_once $root.'/iforum/controller/Utilitarios.php';

    session_start();
    
    $action = null;
    if(isset($_POST['action'])){
        $action  = $_POST['action'];
        $RETORNO = array();
    }
    if($action == 'listf'){
    	$except = $_POST['except'];  
        $count = 0;
        
        if(isset($_POST['usr'])){
            $usr = $_POST['usr'];
            $friends = UsuarioDAO::listByName($usr,$except);
		}else{
            $friends = UsuarioDAO::listAll($except);
            $count   = UsuarioDAO::countFriends($_SESSION['id_user_posts']);    
        }
		foreach ($friends as $key => $value) {
			$id   = $value->id_u;
			$nome = $value->nome;
            $sobrenome = $value->sobrenome;
            $userName = $value->user;
			$img  = $value->img;
			$curso = $value->curso;
			
			$user = " 
					
				<li countfriends='$count' user='$userName' nome='$nome $sobrenome' id='$id' id_comp='".$except.":".$id."' class='start-conversation row'>
                    <div class='col-md-3'>
                        <img src='$img' alt='$nome's photo'>    
                    </div>
                    <div class='col-md-9'>
                        <i class='friend status-off mdi mdi-checkbox-blank-circle-outline pull-right '></i>
                        <p>".$nome." ".$sobrenome."<br><small>$curso </small></p>
                    </div>
                </li>
				 ";
		
            //$user = "<li countFriends='$count' nome='$nome' id='$id' id_comp='".$except.":$id'   class='start-conversation'><a class='a'><div class='row'><!--COLUNA DOS DADOS --><div class='col-xs-9 col-sm-9 col-md-9'><div class='row'><!-- COLUNA DA IMAGEM --><div class='col-xs-2 col-sm-1 col-md-3'><img  class='img-circle' src='$img' ></div><!--COLUNA DA IMAGEM --><!--COLUNA DO NOME E CURSO --><div class='col-xs-10 col-sm-11 col-md-9'><n class='user $user'>$nome $sobrenome</n><br><small>$curso</small></div><!--COLUNA DO DOS DADOS --></div></div><!--COLUNA DOS DADOS --><!--COLUNA DO STATUS --><div class='col-xs-3 col-sm-3 col-md-3'><span id='$id' class='friend pull-right status-off'><i style='font-style:bold; font-weight:800' class='mdi mdi-checkbox-blank-circle-outline'></i></span><br></div><!--COLUNA DO STATUS--></div><!--FIM DA ROW --> </a></li>";
			
            $RETORNO['friends'][] = $user;
		}
        die(json_encode($RETORNO));
    }
    else if($action == 'send'){
    	try{
	        $mensagem   = utf8_decode(strip_tags($_POST['message']));
	        $to         = $_POST['to'];
	        $from       = $_POST['from'];
	        date_default_timezone_set('America/Sao_Paulo');
	        $date = date('d m Y H:i:s');
	        $date_array = explode(' ', $date);
	        $dia    = $date_array[0];
	        $mes    = $date_array[1];
	        $ano    = $date_array[2];
	        $hora   = $date_array[3];
	        $mes = Utilitarios::getMonthName($mes);
	        $data_hora = $dia.' de '.$mes.' de '.$ano.' às '.$hora;
	        $m = new Mensagem($from,$to,$mensagem,$data_hora);
	        MensagemDAO::enviar($m);
	        $count = MensagemDAO::countMessagesBetweenMeAndOther($from,$to);
	    }catch(Exception $e){
	    }
    }


    else if($action == 'history'){
    	$mensagens 	= array();
    	$id_to 		= (int)$_POST['to'];
    	$id_from  	= (int)$_POST['from'];
    	$limit 		= 20;

    	$jan_id 	= 0;
    	$mensagem 	= "";
    	$fotoUser 	= "";
    	$user 		= UsuarioDAO::searchById($id_to);

    	$res   = MensagemDAO::listAllMsgs($id_from,$id_to,$limit);
    	$lastMessageWasSeen = MensagemDAO::userSawTheMessage($id_from,$id_to);
    	if(!empty($res)){
			    	foreach ($res as $key => $value) {
			    		date_default_timezone_set('America/Sao_Paulo');
		                $data_hora = 0;
		                
		                $dia_atual  =  date('d');
		                $hora_atual =  date('H');
		                $min_atual  =  date('i');
		                $seg_atual  =  date('s');
		                
		                $data_array =  explode(" " , $value->data_hora);
		                $mes_post   =  $data_array[2];
		                $dia_post   =  $data_array[0];
		                $hora_array =  explode(":", $data_array[6]);
		                $hora_post  = $hora_array[0];
		                $min_post   = $hora_array[1];
		                $seg_post   = $hora_array[2];
		                if($dia_atual > $dia_post){
		                    $d = $dia_atual-$dia_post;
		                    if($d == 1){
		                        $data_hora = "Ontem às ".$hora_post.":".$min_post;
		                    }else{
		                        $data_hora = $dia_post." de ".$mes_post." às ".$hora_post.":".$min_post;
		                    }

		                }else if($dia_atual == $dia_post){

		                    $h = $hora_atual-$hora_post;
		                    if($h == 0){
		                         $m = $min_atual-$min_post;
		                         if($m == 0){
		                            $data_hora = "agora mesmo";
		                         }else{
		                            $data_hora = $min_atual-$min_post."m";
		                         }
		                    }else{
		                        $data_hora = $hora_atual-$hora_post."h";
		                    }
		                }else{
		                    $data_hora = $dia_post." de ".$mes_post." às ".$hora_post.":".$min_post;
		                }
						if($id_from == $value->id_from){
				    		$jan_id = $value->id_to;
					    }else{
					    	$jan_id 	= $value->id_from;
					    	$user 		= UsuarioDAO::searchById($jan_id);
					    	$fotoUser  	= $user->getImg();
					    }
			    		$emotions 	= array(':)',':(','B)',':D','^^',':poop:');
					    $icons		= array(
					    				'<img src="/iforum/res/imgs/emotions/smile.png">',
					    				'<img src="/iforum/res/imgs/emotions/frown.png">',
					    				'<img src="/iforum/res/imgs/emotions/glasses.png">',
					    				'<img src="/iforum/res/imgs/emotions/grin.png">',
					    				'<img src="/iforum/res/imgs/emotions/kiki.png">',
					    				'<img src="/iforum/res/imgs/emotions/poop.png">',
					    				);

					    $mensagem = $value->mensagem;
				    	$mensagem = str_replace($emotions,$icons,$mensagem);

				    	$mensagens[] = array(
			    			'id' 		=> $value->id,
			    			'id_from'  	=> $value->id_from,
			    			'id_jan' 	=> $jan_id,
			    			'message' 	=> utf8_encode($mensagem),
			    			'nome'		=> $user->getNome(),
			    			'fotoUser' 	=> $fotoUser,
			    			'datahora'  => $data_hora,
			    			'lastMessageWasSeen' => $lastMessageWasSeen
			    		);

				    }//fim do loop buscando por mensagens
					die(json_encode($mensagens));
			    }else{
			    	$user 		= UsuarioDAO::searchById($id_to);
			    	$mensagens[] = array(
			    			"user"    => $user->getNome(),
			    			"message" => "nothing");
			    	die(json_encode($mensagens));
			    }

    }
    
    else if($action == 'checkInitialUsersStatus'){
	    //UsuarioDAO::setLimitOnly($_POST['u'],date("Y-m-d H:i:s",strtotime( "+1 min")));
    }

	else if($action == 'view'){
		$id_to 	 = $_POST['id_to'];
		$id_from = $_POST['id_from'];
		MensagemDAO::updateStatus($id_from,$id_to);
	}
	/*FOLLOW*/
	else if($action == 'follow'){
		$seguidor = $_POST['seguidor'];
		$seguido  = $_POST['seguido'];
		UsuarioDAO::follow($seguidor,$seguido);
	}

	else if($action == 'unfollow'){
		$seguidor = $_POST['seguidor'];
		$seguido  = $_POST['seguido'];
		UsuarioDAO::unfollow($seguidor,$seguido);
	}
	else if($action == 'checkfollow'){
		$resp = array();
        $seguidor = $_POST['seguidor'];
		$seguido  = $_POST['seguido'];
		$check 	  = UsuarioDAO::isFollowing($seguidor,$seguido);
		foreach ($check as $key => $value) {
			$resp[] = array('follower' => $value->seguidor , 'permission' => $value->permissao) ;
        }
        die(json_encode($resp));

	}else if($action == "accept-following"){
        if(isset($_POST['listFriends'])){
        	foreach ($_POST['listFriends'] as $key => $value) {
	        	UsuarioDAO::acceptFollow($value,$_POST['followed']);        	
        	}
        }else{
	        $follower = $_POST['follower'];
	        $followed = $_POST['followed'];
	        UsuarioDAO::acceptFollow($follower,$followed);        	
        }

    }else if($action == "reject-following"){
        $follower = $_POST['follower'];
        $followed = $_POST['followed'];
        UsuarioDAO::rejectFollow($follower,$followed);
    }




    else if($action == "myRequests"){
        $allRequests = array();
        
        $userOnline = $_POST['user'];
        $followRequests = UsuarioDAO::friendRequest($userOnline);
        $followers = array();
        $count = 0;
        foreach($followRequests as $request){
            $count++;
            $ureq = UsuarioDAO::searchById($request->seguidor);
        	$followers[] = array('viewed' => $request->visto, 'notification_id' => $request->id ,  'fid' => $ureq->getId(),'user' =>$ureq->getUserName(), 'usr' => $ureq->getUserName(), 'nome' => $ureq->getNome(), 'sobrenome' => $ureq->getSobrenome(), 'img' => $ureq->getImg(), 'sex' => $ureq->getGenero() );
        }
        	
        /* GET NOTIFICATIONS - FÓRUNS (LIKES)*/
	    $people = PostagemDAO::likesNotSeen($userOnline);
	    $likesNotSeen = array();
	    foreach ($people as $p) {
	        $count++;
	        $u      = UsuarioDAO::searchById($p->id_user);
	        $post   = PostagemDAO::searchById($p->id_postagem);
	        $likesNotSeen[] = array('id' => $p->id, 'name' => $u->getNome(),'sobrenome' => $u->getSobrenome(),'user' => $u->getUserName(), 'img' => $u->getImg(), 'id_postagem' => $post->getId(), 'duvida_postagem' => $post->getDuvida(), 'categoria_postagem' => $post->getCategoria());
	    }
	    /* GET NOTIFICATIONS - FÓRUNS (LIKES) */

	    /* GET NOTIFICATIONS - FÓRUNS (COMMENTS)*/
	    $comments = ComentarioDAO::commentsNotSeen($userOnline);
	    $commentsNotSeen = array();
	    foreach ($comments as $c) {
	        $count++;
	        $u      	= UsuarioDAO::searchById($c->id_user);
	        $comment  	= ComentarioDAO::searchById($c->id);
	        $commentsNotSeen[] = array('id' => $c->id, 'nome' => $u->getNome(),'sobrenome' => $u->getSobrenome(),'user' => $u->getUserName(), 'img' => $u->getImg(),'comentario' => $comment->getComentario(), 'data_comentario' => $comment->getDataHora(), 'id_post_comentario' => $comment->getIdPost());
	    }
	    /* GET NOTIFICATIONS - FÓRUNS (COMMENTS)*/
        $allRequests = array('followRequests' => $followers,'likesNotSeen' => $likesNotSeen, 'commentsNotSeen' => $commentsNotSeen ,'count' => $count);
        die(json_encode($allRequests));
    }



    else if($action == "view_friend_notify"){
        $id = $_POST['id'];
        UsuarioDAO::view_friend_notification($id);
    }
 	/*FOLLOW*/
    /* VERIFICAÇÂO - SE O EMAIL DO CADASTRO JÀ EXISTE */
    else if($action == 'checkIfEmailExists'){
        $email = $_POST['email'];
        $exist = UsuarioDAO::emailExists($email);
        echo $exist;
    }
