<?php
$root = $_SERVER['DOCUMENT_ROOT'];
require_once $root.'/iforum/model/Usuario.php';
require_once $root.'/iforum/DAO/UsuarioDAO.php';
require_once $root.'/iforum/DAO/PostagemDAO.php';
require_once $root.'/iforum/DAO/ComentarioDAO.php';
require_once $root.'/iforum/model/Mensagem.php';
require_once $root.'/iforum/DAO/MensagemDAO.php';
require_once $root.'/iforum/controller/Utilitarios.php';


if(isset($_GET['user'])){
    $userOnline=(int) $_GET[ 'user'];
    $timestamp =($_GET['timestamp']==0) ? time() : strip_tags(trim($_GET[ 'timestamp']));
    $lastid =(isset($_GET[ 'lastid']) && !empty($_GET[ 'lastid'])) ? $_GET[ 'lastid'] : 0;


    $now = date("Y-m-d H:i:s");
    $timeLimit = date("Y-m-d H:i:s",strtotime( "+1 min"));
    UsuarioDAO::setLimitOnly($userOnline,$timeLimit);

    /* GET NOTIFICATIONS - FRIENDS */
    $countNotifications = 0;

    $requests = UsuarioDAO::friendRequest($userOnline);
    $followers = array();
    foreach($requests as $key => $request){
        $countNotifications++;
        $ureq = UsuarioDAO::searchById($request->seguidor);
        $followers[] = array('fid' => $ureq->getId(),'user' =>$ureq->getUserName(), 'nome' => $ureq->getNome(), 'sobrenome' => $ureq->getSobreNome() ,  'img' => $ureq->getImg(), 'sex' => $ureq->getGenero());
    }
    /* GET NOTIFICATIONS - FRIENDS */


    /* GET NOTIFICATIONS - FÓRUNS (LIKES)*/
    $people = PostagemDAO::likesNotSeen($userOnline);
    $likesNotSeen = array();
    foreach ($people as $p) {
        $countNotifications++;
        $u      = UsuarioDAO::searchById($p->id_user);
        $post   = PostagemDAO::searchById($p->id_postagem);   
        $likesNotSeen[] = array('id' => $p->id, 'name' => $u->getNome(),'sobrenome' => $u->getSobrenome(),'user' => $u->getUserName(), 'img' => $u->getImg(), 'id_postagem' => $post->getId(), 'duvida_postagem' => $post->getDuvida(), 'categoria_postagem' => $post->getCategoria());
    }
   /* GET NOTIFICATIONS - FÓRUNS (COMMENTS)*/
    $comments = ComentarioDAO::commentsNotSeen($userOnline);
    $commentsNotSeen = array();
    foreach ($comments as $c) {
        $countNotifications++;
        $u          = UsuarioDAO::searchById($c->id_user);
        $comment    = ComentarioDAO::searchById($c->id);
        $commentsNotSeen[] = array('id' => $c->id, 'nome' => $u->getNome(),'sobrenome' => $u->getSobrenome(),'user' => $u->getUserName(), 'img' => $u->getImg(),'comentario' => $comment->getComentario(), 'data_comentario' => $comment->getDataHora(), 'id_post_comentario' => $comment->getIdPost());
    }
    /* GET NOTIFICATIONS - FÓRUNS */

    /* GET USERS ONLINE  */
    $usersOn = array();
    $users = UsuarioDAO::listAll($userOnline);
    foreach ($users as $key=> $value) {
        /* IF IMPLEMENTAR OS BLOCKS $blocks = explode(',', $value->blocks); if(!in_array($userOnline, $blocks)){ if($now >= $value->limit_time){ } }*/
        if($now >= $value->limit_time){
            $usersOn[] = array('id' => $value->id_u,'status' => 'status-off');
        }else{
            $usersOn[] = array('id' => $value->id_u,'status' => 'status-on');
        }
    }
    /* GET USERS ONLINE  */


    if(empty($timestamp)){
        die(json_encode(array('status' => 'erro')));
    }
    $tempoGasto = 0;
    $lastidQuery = '';
    if(!empty($lastid)){
        $lastidQuery = ' AND id > '.$lastid;
    }
    if($_GET['timestamp'] == 0){
        $verify = MensagemDAO::listAllNotReaded();
    }else{
        $verify = MensagemDAO::listByTimeStamp($timestamp,$lastidQuery);
    }

    $countMsg = 0;
    foreach ($verify as $key) { $countMsg++; }
    if($countMsg <= 0){
        while ($countMsg <= 0) {
            if($countMsg <= 0){
                if($tempoGasto >= 30){
                    die(json_encode(array('status' => 'void', 'lastid' => 0, 'timestamp' => time(),'users' => $usersOn, 'followers' => $followers, 'likesNotSeen' => $likesNotSeen, 'commentsNotSeen' => $commentsNotSeen ,'countNotifications' => $countNotifications)));
                    exit;
                }
                sleep(1);
                /* GET MSGs */
                $verify = MensagemDAO::listByTimeStamp($timestamp,$lastidQuery);
                $countRes = 0;
                foreach ($verify as $key) { $countRes++; }
                $countMsg = $countRes;
                $tempoGasto += 1;
                /* GET MSGs */
            }
        }
    }

    $newMessages = array();
    if($countMsg >= 0){
        $emotions 	= array(':)',':(','B)',':D','^^',':poop:');
        $icons = array('<img src="/iforum/res/imgs/emotions/smile.png">',
                       '<img src="/iforum/res/imgs/emotions/frown.png">',
                       '<img src="/iforum/res/imgs/emotions/glasses.png">',
                       '<img src="/iforum/res/imgs/emotions/grin.png">',
                       '<img src="/iforum/res/imgs/emotions/kiki.png">',
                       '<img src="/iforum/res/imgs/emotions/poop.png">');
    foreach($verify as $key => $value){
        $name = "";
        $fotoUser = "";
        $jan_id = 0;
        if($userOnline == $value->id_from){
            $jan_id = $value->id_to;
        }else{
            $jan_id = $value->id_from;
            $u = UsuarioDAO::searchById($jan_id);
            $name = $u->getNome();
            $fotoUser = $u->getImg();
        }
        $mensagem = $value->mensagem;
        $mensagem = str_replace($emotions,$icons,$mensagem);
        date_default_timezone_set('America/Sao_Paulo');
        $data_hora = 0; $dia_atual = date('d'); $hora_atual = date('H');
        $min_atual = date('i');
        $seg_atual = date('s');
        $data_array = explode(" " , $value->data_hora);
        $mes_post = $data_array[2];
        $dia_post = $data_array[0];
        $hora_array = explode(":", $data_array[6]);
        $hora_post = $hora_array[0];
        $min_post = $hora_array[1];
        $seg_post = $hora_array[2];
        if($dia_atual > $dia_post){
            $d = $dia_atual-$dia_post;
            if($d == 1){
                $data_hora = "Ontem às ".$hora_post.":".$min_post;
            }
            else{
                $data_hora = $dia_post." de ".$mes_post." às ".$hora_post.":".$min_post;
            }
        }else if($dia_atual == $dia_post){
            $h = $hora_atual-$hora_post;
            if($h == 0){
                $m = $min_atual-$min_post;
                if($m == 0){
                    $data_hora = "agora mesmo";
                }else{
                    $data_hora = $min_atual-$min_post."m atrás";
                }
            }else{
                $data_hora = $hora_atual-$hora_post."h atrás";
            }
        }else{
            $data_hora = $dia_post." de ".$mes_post." às ".$hora_post.":".$min_post;
        }

        
        $newMessages[] = array( 'id' => $value->id, 'id_from' => $value->id_from, 'id_to' => $value->id_to , 'jan_id' => $jan_id, 'message' => utf8_encode($mensagem), 'nome' => utf8_encode($name), 'fotoUser' => $fotoUser, 'datahora' => utf8_encode($data_hora));

}//end of foreach
    }
        $lastMsg = end($newMessages);
        $ultimoId = $lastMsg['id'];
        
        die(json_encode( array('status' => 'ok', 'timestamp' => time(), 'lastid' => $ultimoId, 'jan_id' => $jan_id,'dados' => $newMessages, 'users' =>  $usersOn , 'followers' => $followers, 'likesNotSeen' => $likesNotSeen, 'commentsNotSeen' => $commentsNotSeen ,'countNotifications' => $countNotifications)));
}
