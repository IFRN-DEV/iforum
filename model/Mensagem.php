<?php 

class Mensagem{
	private $id			= null;
	private $id_from 	= null;
	private $id_to 		= null;
	private $mensagem 	= null;
	private $data_hora 	= null;


	public function __construct($id_from,$id_to,$mensagem,$data_hora){
		$this->id_from 		= $id_from;
		$this->id_to 		= $id_to;	
		$this->mensagem 	= $mensagem;	
		$this->data_hora 	= $data_hora;	
	}



	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	

	public function getId_from(){
		return $this->id_from;
	}
	
	public function setId_from($id_from){
		$this->id_from = $id_from;
	}
	


	public function getId_to(){
		return $this->id_to;
	}
	
	public function setId_to($id_to){
		$this->id_to = $id_to;
	}



	public function getMensagem(){
		return $this->mensagem;
	}
	
	public function setMensagem($mensagem){
		$this->mensagem = $mensagem;
	}


	public function getDataHora(){
		return $this->data_hora;
	}
	
	public function setDataHora($data_hora){
		$this->data_hora = $data_hora;
	}

}	