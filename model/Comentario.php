<?php  
class Comentario{
	private $id			= null;
	private $id_user 	= null;
	private $id_post 	= null;
	private $comentario	= null;
	private $data_hora 	= null;
	private $visto 		= null;

	public function __construct($id_user,$id_post,$comentario,$data_hora,$visto){
		$this->id_user 		= $id_user;
		$this->id_post 		= $id_post;	
		$this->comentario 	= $comentario;	
		$this->data_hora 	= $data_hora;	
		$this->visto 		= $visto;
	}



	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}

	public function getIdUser(){
		return $this->id_user;
	}
	
	public function setIdUser($id_user){
		$this->id_user = $id_user;
	}


	public function getIdPost(){
		return $this->id_post;
	}
	
	public function setIdPost($id_post){
		$this->id_post = $id_post;
	}


	public function getComentario(){
		return $this->comentario;
	}
	
	public function setComentario($comentario){
		$this->comentario = $comentario;
	}


	public function getDataHora(){
		return $this->data_hora;
	}
	
	public function setDataHora($data_hora){
		$this->data_hora = $data_hora;
	}

	public function getVisto(){
		return $this->visto;
	}
	
	public function setVisto($visto){
		$this->visto = $visto;
	}

}	