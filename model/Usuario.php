<?php 

class Usuario{
	private $id			= null;
	private $user 		= null;
	private $nome 		= null;
	private $sobrenome 	= null;
	private $genero 	= null;
	private $dia 		= null;
	private $mes 		= null;
	private $ano 		= null;
	private $email 		= null;
	private $cidade 	= null;
	private $telefone 	= null;
	private $matricula 	= null;
	private $curso 		= null;
	private $serie 		= null;
	private $senha 		= null;
	private $status 	= null;
	private $img 		= null;
	private $noob 		= null;
	private $bio 		= null;
	private $link 		= null;

	public function Usuario($user,$nome,$sobrenome,$genero,$dia,$mes,$ano,$bio,$link,$email,$cidade,$telefone,
								$matricula,$curso,$serie,$senha,$status,$img,$noob){
		$this->user 		= $user;
		$this->nome 		= $nome;
		$this->sobrenome 	= $sobrenome;
		$this->genero 		= $genero;
		$this->dia 			= $dia;
		$this->mes 			= $mes;
		$this->ano 			= $ano;
		$this->bio 			= $bio;
		$this->link 		= $link;
		$this->email 		= $email;
		$this->cidade 		= $cidade;
		$this->telefone 	= $telefone;
		$this->matricula 	= $matricula;
		$this->curso 		= $curso;
		$this->serie 		= $serie;
		$this->senha 		= $senha;
		$this->status 		= $status;
		$this->img 			= $img;	
		$this->noob 		= $noob;
	}



	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	

	public function getUserName(){
		return $this->user;
	}
	
	public function setUserName($user){
		$this->user = $user;
	}

	public function getNome(){
		return $this->nome;
	}
	
	public function setNome($nome){
		$this->nome = $nome;
	}


	public function getSobrenome(){
		return $this->sobrenome;
	}
	
	public function setSobrenome($sobrenome){
		$this->sobrenome = $sobrenome;
	}


	public function getGenero(){
		return $this->genero;
	}
	
	public function setGenero($genero){
		$this->genero = $genero;
	}


	public function getDia(){
		return $this->dia;
	}
	
	public function setDia($dia){
		$this->dia = $dia;
	}


	public function getMes(){
		return $this->mes;
	}
	
	public function setMes($mes){
		$this->mes = $mes;
	}


	public function getAno(){
		return $this->ano;
	}
	
	public function setAno($ano){
		$this->ano = $ano;
	}


	public function getEmail(){
		return $this->email;
	}
	
	public function setEmail($email){
		$this->email = $email;
	}


	public function getCidade(){
		return $this->cidade;
	}
	
	public function setCidade($cidade){
		$this->cidade = $cidade;
	}


	public function getTelefone(){
		return $this->telefone;
	}
	
	public function setTelefone($telefone){
		$this->telefone = $telefone;
	}


	public function getMatricula(){
		return $this->matricula;
	}
	
	public function setMatricula($matricula){
		$this->matricula = $matricula;
	}


	public function getCurso(){
		return $this->curso;
	}
	
	public function setCurso($curso){
		$this->curso = $curso;
	}


	public function getSerie(){
		return $this->serie;
	}
	
	public function setSerie($serie){
		$this->serie = $serie;
	}


	public function getSenha(){
		return $this->senha;
	}
	
	public function setSenha($senha){
		$this->senha = $senha;
	}


	public function getStatus(){
		return $this->status;
	}
	
	public function setStatus($status){
		$this->status = $status;
	}


	public function getImg(){
		return $this->img;
	}
	
	public function setImg($img){
		$this->img = $img;
	}


	public function getNoob(){
		return $this->noob;
	}
	
	public function setNoob($noob){
		$this->noob = $noob;
	}

	public function setBio($bio){
		$this->bio = $bio;
	}
	public function getBio(){
		return $this->bio;
	}

	public function setLink($link){
		$this->link = $link;
	}
	public function getLink(){
		return $this->link;
	}

}