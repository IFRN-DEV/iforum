<?php 

class Postagem{
	private $id			= null;
	private $id_user 	= null;
	private $duvida 	= null;
	private $categoria 	= null;
	private $data_hora 	= null;


	public function __construct($id_user,$duvida,$categoria,$data_hora){
		$this->id_user 		= $id_user;
		$this->duvida 		= $duvida;	
		$this->categoria 	= $categoria;	
		$this->data_hora 	= $data_hora;		
	}



	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	

	public function getId_user(){
		return $this->id_user;
	}
	
	public function setId_user($id_user){
		$this->id_user = $id_user;
	}
	


	public function getDuvida(){
		return $this->duvida;
	}
	
	public function setDuvida($duvida){
		$this->duvida = $duvida;
	}



	public function getCategoria(){
		return $this->categoria;
	}
	
	public function setCategoria($categoria){
		$this->categoria = $categoria;
	}


	public function getDataHora(){
		return $this->data_hora;
	}
	
	public function setDataHora($data_hora){
		$this->data_hora = $data_hora;
	}

}	