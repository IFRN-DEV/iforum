 <nav class='navbar-fixed-top'>
  <div id='navbar-main'>
    <ul>
      <div class='row'>
        <div class="col-lg-2 col-md-2 col-sm-2">
          <li> 
            <div class="row">
                <div class="col-xs-6 ">
                  <a href='/iforum'><b><i style='font-size:30px; position:absolute; top:18px' class='mdi mdi-forum'></i></b></a>
                </div>
                <div class="col-xs-6">
                  <button  class='pull-right mdi mdi-menu btn'></button>
                </div>
            </div> 
           </li>
        </div>
        <div class="hideIt col-lg-8 col-md-8 col-sm-8">
            <li><a href='/iforum/<?= $_SESSION['userName'] ?>'>Perfil</a></li>
            <li><a href='/iforum/salas'>Fóruns</a></li>
            <li><a href='/iforum/competicoes'>Competições</a></li>
            <li><a href='/iforum/bestStudent'>Aluno Destaque</a></li>
        </div>
        <div class="hideIt col-lg-2 col-md-2 col-sm-2">
          <li class='active has-sub'><a href='#'><img width="22" height="22" src="<?= $_SESSION['img'] ?>" class='img-circle' > <?= $_SESSION['nome'] ?></a>
              <ul>
                <li class='has-sub'><a href='/iforum/config'> <i class='mdi mdi-settings'></i> Configurações</a></li>
                <li class='has-sub'><a href='/iforum/logout/<?= $_SESSION['matricula'] ?>'> <i class='mdi mdi-logout'></i> Sair</a></li>
              </ul>
            </li>
        </div>
      </div> 
    </ul>
  </div>
</nav> 


