<span class='profileUser' profileUser='<?=$user?>'></span>
<div class="col-lg-2 col-md-2 profile-bar visible-lg visible-md fix-padding-error" >
                    <div class="profile-info">
                        <form>
                            <?php if($isMe){ ?>
                            <figure>
                                <img src="<?= $img ?>" id='profile-img-lg'>
                                <figcaption  data-toggle="modal" data-target="#changePhoto" >
                                    <i class='mdi mdi-camera'></i><br>
                                </figcaption>
                            </figure>
                            <?php }else{ ?>
                                <img src="<?= $img ?>" id='profile-img-lg'>
                            <?php } ?>
                        </form>
                        <div class='info'>
                            <p>  <?= $nome ." ". $sobrenome ?></p>
                            <h6> <span style='margin-right:6px; font-size:16px' class='mdi mdi-school icon'></span><?= $curso." (".$serie." ano)"?></h6>
                            <h6> <span style='margin-right:6px; font-size:16px' class='mdi mdi-email icon'></span><?= $email ?></h6>

                            <?php if(!$isMe){ ?>
                            <div class="friendsdiv">
                                <button id='<?= $_SESSION['id'].":".$id ?>' class='follow btn btn-follow'></button>
                            </div>
                            <?php } ?>
                            <br>
                        </div>
                    </div>
                    <div class="profile-nav">
                        <ul class="nav">
                            <li><a href="/iforum/<?=$user?>"> <i class='mdi mdi-account'></i> Perfil</a></li>
                            <li><a href="/iforum/<?=$user?>/duvidas"> <i class='mdi mdi-message'></i>  Dúvidas<span class="badge  pull-right count-posts">0</span></a></li><!-- <li><a href="">Fóruns<span class="badge pull-right">0</span></a></li> -->
                            <li><a href="/iforum/<?=$user?>/grupos"> <i class='mdi mdi-google-circles-extended'></i> Grupos<span class="badge pull-right count-groups">0</span></a></li>
                            <li><a href="/iforum/<?=$user?>/amigos"> <i class='mdi mdi-account-multiple'></i> Amigos<span class="badge pull-right count-friends">0</span></a></li>
                            <?php if($isMe){ ?>
                                <li><a href="/iforum/<?=$user?>/notificacoes"> <i class='mdi mdi-bell'></i> Notificações<span class="badge pull-right count-notifications">0</span></a></li>
                            <?php }?>
                            <!--<hr style='border:1px solid #555; margin:4px 10px'>-->
                        </ul>
                    </div>
                </div>
                <!-- PERFIL PARA MOBILE -->
                <div class="col-lg-2 col-md-2 profile-bar visible-sm visible-xs fix-padding-error" >
                    <div class="profile-info mobile">
                        <form>
                            <?php if($isMe){ ?>
                                <img src="<?= $img ?>" id='profile-img-sm' class='img-circle' data-toggle="modal" data-target="#changePhoto" >
                            <?php }else{ ?>
                                <img src="<?= $img ?>" id='profile-img-sm' class='img-circle'>
                            <?php } ?>
                        </form>
                         <div class='info'>
                            <p>  <?= $nome ." ". $sobrenome ?></p>
                            <h6> <span style='margin-right:6px; font-size:16px;' class='mdi mdi-school icon'></span><?= $curso." (".$serie." ano)"?></h6>
                            <h6> <span style='margin-right:6px; font-size:16px;' class='mdi mdi-email icon'></span><?= $email ?></h6>

                            <?php if(!$isMe){ ?>
                            <div class="friendsdiv">
                                <button id='<?= $_SESSION['id'].":".$id ?>' class='follow btn btn-follow'></button>
                            </div>
                            <?php } ?>
                            <br>
                        </div>
                    </div>
                    <div class="profile-nav">
                         <ul class="nav">
                            <li><a href="/iforum/<?=$user?>">Perfil</a></li>
                            <li><a href="/iforum/<?=$user?>/duvidas">Dúvidas<span class="badge pull-right count-posts">0</span></a></li><!-- <li><a href="">Fóruns<span class="badge pull-right">0</span></a></li> -->
                            <li><a href="/iforum/<?=$user?>/grupos">Grupos<span class="badge pull-right count-groups">0</span></a></li>
                            <li><a href="/iforum/<?=$user?>/amigos">Amigos<span class="badge pull-right count-friends">0</span></a></li>
                            <?php if($isMe){ ?>
                                <li><a href="/iforum/<?=$user?>/notificacoes">Notificações<span class="badge pull-right count-notifications">0</span></a></li>
                            <?php } ?>
                            <!--<hr style='border:1px solid #555; margin:4px 10px'>-->
                        </ul>
                    </div>
                </div>