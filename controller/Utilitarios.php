<?php 

	class Utilitarios{
    	
    	public static function getMonthNumber($mon) {
            $months = array("Jan", "Fev", "Mar", "Abr", "Maio", "Jun", "Jul",
                    "Ago", "Set", "Out", "Nov", "Dez");

            for ($i = 0; $i < count($months); $i++) {
                if($months[$i] == $mon){
                    return $i+1;
                }
            }
            return 0;
        }
        public static function getMonthName($mon) {
            $months = array("jan", "fev", "mar", "abr", "maio", "jun", "jul",
                    "ago", "set", "out", "nov", "dez");
            if ($mon >= 0 && $mon <= count($months)) {
                return $months[$mon - 1];
            } else {
                return "invalid month";
            }
        }
        
        public static function getMonthFullName($mon) {
            $months = array("Jan", "Fev", "Mar", "Abr", "Maio", "Jun", "Jul",
                    "Ago", "Set", "Out", "Nov", "Dez");
            if ($mon >= 0 && $mon < count($months)) {
                return $months[$mon - 1];
            } else {
                return "invalid month";
            }
        }
     
        /*public static function howOldAreYou($d,$m,$y){
            $date        =  new Date(System.currentTimeMillis()).toString();
            $dateArray   = explode(" ",$date);
            $y 			 = $dateArray[5] - $y;
            if(     ($m <= getMonthNumber($dateArray[1]) &&  $d <= ($dateArray[2]) ||
                    ($m < getMonthNumber($dateArray[1])  &&  $d <= ($dateArray[2]) || 
                    ($m < getMonthNumber($dateArray[1])  &&  $d >  ($dateArray[2]))) {
                return $y;
            }
            
            return $y-1;
        }*/

   
        public static function removeAcentos($str) {
          $str = strtolower($str);
          $str = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $str));
          return $str;
        }

        public static function tradutorDeMes($mes){
            $num_mes  = Utilitarios::getMonthNumber($mes);
            $name_mes = Utilitarios::getMonthName($num_mes);
            return $name_mes;
        }


	} 