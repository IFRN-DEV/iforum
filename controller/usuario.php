<?php 
            include_once '../DAO/MensagemDAO.php'; 
            include_once '../DAO/ComentarioDAO.php'; 
            include_once '../DAO/PostagemDAO.php'; 
            include_once '../DAO/UsuarioDAO.php'; 
            include_once '../controller/Utilitarios.php'; 
            include_once '../model/Usuario.php';   
            /* CADASTRO */
            if(isset($_POST['Cadastro'])){
                $nome         = $_POST["nome"];
                $sobrenome    = $_POST["sobrenome"];
                $genero       = $_POST["genero"];
                $dia          = $_POST["dia"];
                $mes          = $_POST["mes"];
                $ano          = $_POST["ano"];
                $email        = $_POST["email"];
                $cidade       = $_POST["cidade"];
                $telefone     = $_POST["telefone"];
                $matricula    = $_POST["matricula"];
                $curso        = $_POST["curso"];
                $serie        = $_POST["serie"];
                $senha        = $_POST["senha"];
                $senha_conf   = $_POST["senha_conf"];
                $bio          = "Sem biografia...";
                $link         = "Web Site / Rede Social"; 
                /* Se senhas forem iguais, o usuário será cadastrado... */
                if($senha == $senha_conf){
                    $user_name = explode("@",$email)[0];
                    $user_name = str_replace(".","_",$user_name);
                    $user_name = Utilitarios::removeAcentos($user_name."".$sobrenome."".$dia);
                    $user = new Usuario($user_name,$nome,$sobrenome,$genero, $dia, $mes, $ano,
                                        $bio,$link,$email, $cidade, $telefone, $matricula, $curso,
                                        $serie, $senha,0,"/iforum/res/imgs/user.png",0);
                   UsuarioDAO::save($user);
                   /*  SELECIONAR O CARA PELA MATRICULA */
                   $u = UsuarioDAO::searchByMat($matricula);

                    /* Redirecionar */
                    ?>      
                    <form name="form_login" action="/iforum/cadastro/foto" method="post" >
                        <input type="hidden" name="id_user" value="<?= $u->getId() ?>">
                        <input type="hidden" name="UPLOAD_IMG" >
                    </form>
                    <script>
                        document.form_login.submit();
                    </script>
                    <?php
                        
                }else{
                    ?>
                    <form name="formpass" method="post" action="/iforum/cadastro">
                        <input type="hidden" name="incompatiblepass" value="incompatiblepass"> 
                    </form>
                    <script>document.formpass.submit();</script>
                    <?php  
                } 
            /* LOGIN */         
            }else if(isset($_POST['Login'])){
                $matricula  = $_POST['matricula'];
                $senha      = $_POST['senha'];   
                $user       = UsuarioDAO::authenticate($matricula,$senha);
                if($user != null){
                    if($user->getStatus() != 0){
                        session_start();
                        $_SESSION['success_login'] = true;
                        $_SESSION['id']         = $user->getId();
                        $_SESSION['userName']   = $user->getUserName();
                        $_SESSION['nome']       = $user->getNome();
                        $_SESSION['sobrenome']  = $user->getSobrenome();
                        $_SESSION['genero']     = $user->getGenero();
                        $_SESSION['dia']        = $user->getDia();
                        $_SESSION['mes']        = $user->getMes();
                        $_SESSION['ano']        = $user->getAno();
                        $_SESSION['email']      = $user->getEmail();
                        $_SESSION['cidade']     = $user->getCidade();
                        $_SESSION['telefone']   = $user->getTelefone();
                        $_SESSION['matricula']  = $user->getMatricula();
                        $_SESSION['curso']      = $user->getCurso();
                        $_SESSION['serie']      = $user->getSerie();
                        $_SESSION['senha']      = $user->getSenha();
                        $_SESSION['status']     = $user->getStatus();
                        $_SESSION['img']        = $user->getImg();
                        UsuarioDAO::setTimeLimit($user->getId());
                    /* Redirecionar */
                    ?>
                       <form name="form_login_success" action="/iforum/<?= $user->getUserName()?>" method="post" >
                            <input type="hidden" name="login_done" >
                        </form>
                        <script>document.form_login_success.submit();</script>  
                    <?php  
                    }else{
                        ?>
                        <form name="form_login_usrNotConf" action="/iforum/<?= $user->getNome() ?>/nao_confirmado" method="post" >
                        <input type='hidden' name='img' value='<?=$user->getImg()?> '>   
                        </form>
                        <script>document.form_login_usrNotConf.submit();</script>
                        <?php  
                    }
                }else{
                     ?>
                       <form name="form_login_wrong" action="/iforum/login" method="post" >
                            <input type="hidden" name="incorrectUser" >
                        </form>
                        <script>
                            document.form_login_wrong.submit();
                        </script>  
                    <?php 
                }


            }else if(isset($_POST['UPLOAD_IMG'])){
                
                $root = $_SERVER['DOCUMENT_ROOT'];
                
                $id = $_POST['id_user'];

                $dir_tmp    = $_FILES["img"]["tmp_name"];
                $img_name   = $_FILES["img"]["name"];
                $img_name   = str_replace(" ","_",$img_name);

                $dir_final  = $root."/iforum/users/$id/";
                
                if(!is_dir($dir_final)){
                    mkdir($dir_final, 0777, true);    
                    $dir_final  = $dir_final."imgs";
                    mkdir($dir_final, 0777, true);
                    $move_dir   = $dir_final."/".$img_name;
                    move_uploaded_file($dir_tmp, $move_dir);
                }else{
                    $address    = $dir_final."/imgs/".$img_name;    
                    if(isset($address)){
                        $img_name   = $img_name.$img_name; 
                        $address    = $dir_final."/imgs/".$img_name;;    
                    }
                    $moved      =  move_uploaded_file($dir_tmp, $address);                            
                }
                $u = UsuarioDAO::searchById($id);    
                $image = "/iforum/users/$id/imgs/".$img_name;
                UsuarioDAO::uploadImage($image,$id);
                ?>
                <form name="form_upload_success" action="usuario.php" method="POST">
                    <input type="hidden" name="matricula"   value="<?= $u->getMatricula() ?>">
                    <input type="hidden" name="senha"       value="<?= $u->getSenha() ?>">
                    <input type="hidden"  name="Login">
                </form> 
                <script> document.form_upload_success.submit(); </script> 
            <?php }else if(isset($_POST['xERASEALLDATAx'])){
                    $id = $_POST['u'];
            }
        else{  header("Location: /iforum/login"); }?>
    