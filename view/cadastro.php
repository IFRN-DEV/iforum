<!DOCTYPE html>
<html>
    <head>
        <title>Cadastro</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" href="/iforum/res/lib/bootstrap/css/bootstrap.min.css">  
        <link rel="stylesheet" href="/iforum/res/lib/material-icons/css/materialdesignicons.min.css">  
        <link rel="stylesheet" href="/iforum/res/css/main.css">
    </head>
        <!-- MODALs -->
         <div id="emailAlreadyExist" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Email já existe. </h4>
                    </div>
                    <div class="modal-body">
                        <h5 style='text-align:left'>Hey man... Esse email já existe. Tente novamente! VLW</h5>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn btn-default' data-dismiss='modal'>Ok</button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        
        
           
           
            <div id="incompatiblepass" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Senhas não conferem!! </h4>
                    </div>
                    <div class="modal-body">
                        <h5 style='text-align:left'>Tente digitar senhas iguais...</h5>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn btn-default' data-dismiss='modal'>Ok</button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->    
        <!-- MODALs -->
    <body class='body-initial'>
        <div class="container"><!-- Conteúdo_da_Página --> 
            <div class="row"><!-- Linha(Divisão_em_Colunas) -->
                <h1>iForum</h1><!-- Título -->
                <h3> Cadastre-se no iForum</h3><!-- Subtítulo --> 
                <div class="col-md-3"></div><!-- Coluna_Encher-Linguiça -->
                <div class="col-md-6"><!-- Coluna_Formulário --> 
                    <div class="box"><!-- Caixinha_Cadastro --> 
                        <!-- Form_Cadastro -->
                        <form id="form-cadastro" action="/iforum/controller/usuario.php" method="POST" >
                            <!-- Input´s_Nome_Sobrenome -->
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="group">
                                        <input type="text" name="nome" class="input-flat" required>        
                                        <label>Nome</label>
                                        <span class='bar'></span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="group">
                                        <input type="text"  name="sobrenome" class="input-flat" required>
                                        <label>Sobrenome</label>
                                        <span class='bar'></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <select name="genero" class="input">
                                    <option value="">Gênero...</option>
                                    <option value="m">Masculino</option>
                                    <option value="f">Feminino</option>
                                    <option value="o">Outro</option>    
                                </select> 
                            </div>
                            <div class="col-md-6">
                                <div class="row">    
                                    <div style='padding:0 1px' class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
                                        <select name="dia" class="input" >
                                            <option selected="1">Dia</option>
                                            <option value="1">01</option>
                                            <option value="2">02</option>
                                            <option value="3">03</option>
                                            <option value="4">04</option>
                                            <option value="5">05</option>
                                            <option value="6">06</option>
                                            <option value="7">07</option>
                                            <option value="8">08</option>
                                            <option value="9">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            </select>
                                    </div>
                                    <div style='padding:0 1px' class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
                                        <select name="mes" class='input' >
                                        <option selected="1">Mês</option>
                                        <option value="1">Janeiro</option>
                                        <option value="2">Fevereiro</option>
                                        <option value="3">Março</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Maio</option>
                                        <option value="6">Junho</option>
                                        <option value="7">Julho</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Setembro</option>
                                        <option value="10">Outubro</option>
                                        <option value="11">Novembro</option>
                                        <option value="12">Dezembro</option></select>
                                    </div>
                                    <div style='padding:0 1px' class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
                                        <select name="ano" class='input' >
                                            <option selected="1">Ano</option>
                                            <option value="2016">2016</option>
                                            <option value="2015">2015</option>
                                            <option value="2014">2014</option>
                                            <option value="2013">2013</option>
                                            <option value="2012">2012</option>
                                            <option value="2011">2011</option>
                                            <option value="2010">2010</option>
                                            <option value="2009">2009</option>
                                            <option value="2008">2008</option>
                                            <option value="2007">2007</option>
                                            <option value="2006">2006</option>
                                            <option value="2005">2005</option>
                                            <option value="2004">2004</option>
                                            <option value="2003">2003</option>
                                            <option value="2002">2002</option>
                                            <option value="2001">2001</option>
                                            <option value="2000">2000</option>
                                            <option value="1999">1999</option>
                                            <option value="1998">1998</option>
                                            <option value="1997">1997</option>
                                            <option value="1996">1996</option>
                                            <option value="1995">1995</option>
                                            <option value="1994">1994</option>
                                            <option value="1993">1993</option>
                                            <option value="1992">1992</option>
                                            <option value="1991">1991</option>
                                            <option value="1990">1990</option>
                                            <option value="1989">1989</option>
                                            <option value="1988">1988</option>
                                            <option value="1987">1987</option>
                                            <option value="1986">1986</option>
                                            <option value="1985">1985</option>
                                            <option value="1984">1984</option>
                                            <option value="1983">1983</option>
                                            <option value="1982">1982</option>
                                            <option value="1981">1981</option>
                                            <option value="1980">1980</option>
                                            <option value="1979">1979</option>
                                            <option value="1978">1978</option>
                                            <option value="1977">1977</option>
                                            <option value="1976">1976</option>
                                            <option value="1975">1975</option>
                                            <option value="1974">1974</option>
                                            <option value="1973">1973</option>
                                            <option value="1972">1972</option>
                                            <option value="1971">1971</option>
                                            <option value="1970">1970</option>
                                            <option value="1969">1969</option>
                                            <option value="1968">1968</option>
                                            <option value="1967">1967</option>
                                            <option value="1966">1966</option>
                                            <option value="1965">1965</option>
                                            <option value="1964">1964</option>
                                            <option value="1963">1963</option>
                                            <option value="1962">1962</option>
                                            <option value="1961">1961</option>
                                            <option value="1960">1960</option>
                                            <option value="1959">1959</option>
                                            <option value="1958">1958</option>
                                            <option value="1957">1957</option>
                                            <option value="1956">1956</option>
                                            <option value="1955">1955</option>
                                            <option value="1954">1954</option>
                                            <option value="1953">1953</option>
                                            <option value="1952">1952</option>
                                            <option value="1951">1951</option>
                                            <option value="1950">1950</option>
                                            <option value="1949">1949</option>
                                            <option value="1948">1948</option>
                                            <option value="1947">1947</option>
                                            <option value="1946">1946</option>
                                            <option value="1945">1945</option>
                                            <option value="1944">1944</option>
                                            <option value="1943">1943</option>
                                            <option value="1942">1942</option>
                                            <option value="1941">1941</option>
                                            <option value="1940">1940</option>
                                            <option value="1939">1939</option>
                                            <option value="1938">1938</option>
                                            <option value="1937">1937</option>
                                            <option value="1936">1936</option>
                                            <option value="1935">1935</option>
                                            <option value="1934">1934</option>
                                            <option value="1933">1933</option>
                                            <option value="1932">1932</option>
                                            <option value="1931">1931</option>
                                            <option value="1930">1930</option>
                                            <option value="1929">1929</option>
                                            <option value="1928">1928</option>
                                            <option value="1927">1927</option>
                                            <option value="1926">1926</option>
                                            <option value="1925">1925</option>
                                            <option value="1924">1924</option>
                                            <option value="1923">1923</option>
                                            <option value="1922">1922</option>
                                            <option value="1921">1921</option>
                                            <option value="1920">1920</option>
                                            <option value="1919">1919</option>
                                            <option value="1918">1918</option>
                                            <option value="1917">1917</option>
                                            <option value="1916">1916</option>
                                            <option value="1915">1915</option>
                                            <option value="1914">1914</option>
                                            <option value="1913">1913</option>
                                            <option value="1912">1912</option>
                                            <option value="1911">1911</option>
                                            <option value="1910">1910</option>
                                            <option value="1909">1909</option>
                                            <option value="1908">1908</option>
                                            <option value="1907">1907</option>
                                            <option value="1906">1906</option>
                                            <option value="1905">1905</option>
                                            <option value="1904">1904</option>
                                            <option value="1903">1903</option>
                                            <option value="1902">1902</option>
                                            <option value="1901">1901</option>
                                            <option value="1900">1900</option></select>
                                    </div>
                                </div>
                            </div>   
                        </div>                                
                        <br><br>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="group">
                                    <input type="email" name="email"  class="input-flat" required>
                                    <label>Email</label>
                                    <span class='bar'></span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="group">
                                    <input type="text" name="cidade" class="input-flat" required>
                                    <label>Cidade</label>
                                    <span class='bar'></span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="group">
                                    <input id="fone" onkeypress="format('\##\ #####-####', this)"  maxlength="14" type="text" name="telefone" class="input-flat" required>
                                    <label>Telefone</label>
                                    <span class='bar'></span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="group">
                                    <input type="text" name="matricula" class="input-flat" required>
                                    <label>Matricula</label>
                                    <span class='bar'></span>
                                </div>
                            </div>
                        </div>

                         <br>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="group">
                                    <select name="curso" class="input" required>
                                        <option value="">Curso</option>
                                        <option value="Informática">Informática</option>
                                        <option value="Eletrotécnica">Eletrotécnica</option>
                                        <option value="Administração">Administração</option>
                                        <option value="Cooperativismo">Cooperativismo</option>
                                        <option value="Física">Física</option>
                                        <option value="Energias Renováveis">Energias Renováveis</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="group">
                                    <select name="serie" class="input" required>
                                        <option value="">Série</option>
                                        <option value="1º">1º ano</option>
                                        <option value="2º">2º ano</option>
                                        <option value="3º">3º ano</option>
                                        <option value="4º">4º ano</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br><br>

                         <div class="row">
                            <div style='padding-right:0' class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <div class="group">
                                    <input id='pass1' type="password"  name="senha" class="input-flat" required> 
                                    <label>Senha</label>
                                    <span class='bar'></span> 
                                </div>
                            </div>
                            <!-- PASS WATCH -->
                            <center>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <i  class='mdi mdi-eye showPass passWatcher'></i>                            
                                </div>
                            </center>
                            <!-- PASS WATCH -->
                            <div  style='padding-left:0' class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <div class="group">
                                    <input id='pass2' type="password"  name="senha_conf" class="input-flat" required>                
                                    <label>Confirmar senha</label>
                                    <span class='bar'></span>
                                 </div>
                            </div>
                        </div>
                        <!-- fim_do_Input_Senha_Confirm-Senha --> 
                        <br><br>
                        <!-- Button_Salvar -->
                        <input type="button"  name="Cadastro"   class="btn btn-success form save-user"    value="Salvar">
                        <!-- Button_Cancelar -->
                        <a href="login"><input type="button"     class="btn btn-danger  form pull-right"   value="Cancelar"></a>
                    </form>
                    <!-- Fim_do_Form_Cadastro -->
                </div><!-- Fim_da_Caixinha_Cadastro -->   
            </div><!-- Fim_da_Coluna_Formulário_Cadastro -->
           
            <div class="col-md-3"></div><!-- Coluna_Encher-Linguiça-->               
        </div><!-- Fim_da_Linha(Dicvisão_em_Colunas) -->   
    </div><!-- Conteúdo_da_Página -->

      
           
        
        
        <script src="/iforum/res/lib/jquery/jquery.all.js"></script>
        <script src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script>    
            //$("#fone").mask("(00) 0000-00009");
            $("body").on('blur',"input[type='email']",function(){
                e       = $(this);
                email   = e.val();
                $.ajax({
                    url:"/iforum/pages/ajax_pages/usr_m.php",
                    type:'post',
                    data:{action:'checkIfEmailExists',email:email},
                    dataType:'json',
                    success:function(resp){
                        if(resp > 0){
                            e.attr('exist','true')
                            e.css('border-color','#E03B50');
                            $("#emailAlreadyExist").modal('show');
                        }else{
                            e.attr('exist','false');
                            e.css('border-color','#DDD');
                            $('.save-user').attr('type','submit');
                        }
                    }
                });
            });
            $('body').on('click','.mdi-eye',function(){
                $("input[type='password']").attr('type','text').addClass('PassShowed'); 
                $(this).addClass("mdi-eye-off").removeClass('mdi-eye');
            });
            $('body').on('click','.mdi-eye-off',function(){
                $(".PassShowed").attr('type','password');
                $(this).addClass("mdi-eye").removeClass('mdi-eye-off');
            });
            
            var pass1,pass2;
            $('body').on('blur',"#pass1",function(){
                pass1 = $(this).val();
                if(pass1 != pass2){
                    $('.save-user').attr('type','button');
                    $('.save-user').addClass('wrongPass');
                }else{
                    $('.save-user').attr('type','submit');
                    $('.save-user').removeClass('wrongPass');
                }
            });
            $('body').on('blur',"#pass2",function(){
                pass2 = $(this).val();
                if(pass1 != pass2){
                    $('.save-user').attr('type','button');
                    $('.save-user').addClass('wrongPass');
                    $("#incompatiblepass").modal('show');
                }else{
                    $('.save-user').attr('type','submit');
                    $('.save-user').removeClass('wrongPass');
                }
            });
            $('body').on('click','.wrongPass',function(){
                $("#incompatiblepass").modal('show');
            });
            /*  Formatação do campo telefone onkeypress="format('##-#####-####', this)" */
            function format(mascara, e){
                  var i = e.value.length;
                  var out = mascara.substring(0,1);    
                  var text = mascara.substring(i);
                  if (text.substring(0,1) != out){
                        e.value += text.substring(0,1);
                  }	  
            }    
        </script>
        <script>
            $(document).ready(function(){
                $('.input-flat').blur(function(){
                    if($(this).val()  !==  '') $(this).addClass('used');
                    else $(this).removeClass('used');
                });
            });
        </script>
    </body>
</html>
