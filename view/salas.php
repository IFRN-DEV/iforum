<?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        require_once ($root."/iforum/DAO/UsuarioDAO.php");
        require_once ($root."/iforum/DAO/PostagemDAO.php");
        require_once ($root."/iforum/model/Usuario.php");
        session_start();
        if(isset($_SESSION['id'])){
            $id = $_SESSION["id"];
            $_SESSION['id_user_posts'] = $id;
            $u = UsuarioDAO::searchById($id);
            $user        = $u->getNome();
            $sobrenome   = $u->getSobrenome();
            $mat         = $u->getMatricula();
            $img         = $u->getImg();
            $serie       = $u->getSerie();
            $curso       = $u->getCurso();
            $status      = $u->getStatus();
        }else{header("Location: /iforum/login");}
    ?>
<!DOCTYPE html>
    <html>
        <head>
            <!-- TAG DO CHROME MOBILE -->
            <meta name="theme-color" content="#52906F">
            <!-- TAG DO CHROME MOBILE -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
            <title><?= $user ?></title>
            <link rel="stylesheet" href='/iforum/res/lib/bootstrap/css/bootstrap.min.css'>

            <link rel="stylesheet" href='/iforum/res/lib/material-icons/css/materialdesignicons.min.css'>
            <link rel="stylesheet" href='/iforum/res/css/main.css'>
        </head>
        <body>
            <!-- MENU -->
            <?php include ($root."/iforum/templates/navbar.php"); ?>
            <!-- MENU -->
            <div class="col-md-10 categorias dashboard fix-padding-error">
                   <div class="row">
                   		<div class="col-md-3">
                        <div id="informatica" class="box informatica categoria">
                          <h1>Informática</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('informatica'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                          <!--<img src="/iforum/res/imgs/categories/info.png">-->
                        </div>
                   		</div>
                      <div  class="col-md-3">
                        <div id="fisica" class="box fisica categoria">
                          <h1>Física</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('fisica'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div id="matematica" class="box matematica categoria">
                          <h1>Matemática</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('matematica'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div id="historia" class="box historia categoria">
                          <h1>História</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('historia'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"></i> <?=$count?> </h6></small>
                        </div>
                      </div>
                   </div>
					         <div class="row">
                   		<div class="col-md-3">
                        <div id="eletronica" class="box eletronica categoria">
                          <h1>Eletronica</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('eletronica'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                      </div>
                      <div class="col-md-3">
                   			<div id="quimica" class="box quimica categoria">
                          <h1>Química</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('quimica'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                   		</div>
                      <div class="col-md-3">
                        <div id="linguagens" class="box linguagens categoria">
                          <h1>Linguagens</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('linguagens'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div id="filosofia" class="box filosofia categoria">
                          <h1>Filosifia</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('filosofia'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                      </div>
                   </div>
                   <div class="row">
                   		<div class="col-md-3">
                        <div id="administracao" class="box administracao categoria">
                          <h1>ADM</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('administracao'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                      </div>
                   		<div class="col-md-3">
                   			<div id="biologia" class="box biologia categoria">
                          <h1>Biologia</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('biologia'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                   		</div>
                   		<div class="col-md-3">
                   			<div id="geografia" class="box geografia categoria">
                          <h1>Geografia</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('geografia'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                   		</div>
                      <div class="col-md-3">
                        <div id="sociologia" class="box sociologia categoria">
                          <h1>Sociologia</h1>
                          <?php $count = PostagemDAO::countForumsByCategory('sociologia'); ?>
                          <small style='color:#999;' class='pull-left'><h6><i class='mdi mdi-forum' style="color:#999"  ></i> <?= $count  ?> </h6></small>
                        </div>
                      </div>
                   </div>
              </div>

            <!-- CHAT -->
            <?php include_once '../templates/chat.php'; ?>
            <!-- CHAT -->
            
            <!-- janelinhas -->
            <div id="janelas">
                <!--JANELAS CARREGARÃO AQUI... -->
            </div>
            <!-- janelinhas -->



        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>        -->
        <script type="text/javascript" src="/iforum/res/lib/jquery/jquery.all.js"></script>
        <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/iforum/res/js/usr_p.js"></script>
        <script type="text/javascript" src="/iforum/res/js/chat.js"></script>
        <script type="text/javascript" src="/iforum/res/js/play_sound.js"></script>


        <script>
          $(document).ready(function(){
              $(".categoria").click(function(){
                window.location.href="salas/"+$(this).attr('id');
              });
          });
        </script>
        </body>
    </html>
