<?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        require_once ($root.'/iforum/DAO/PostagemDAO.php');
        require_once ($root."/iforum/DAO/UsuarioDAO.php");
        require_once ($root."/iforum/model/Usuario.php");
        require_once ($root."/iforum/DAO/ComentarioDAO.php");
        session_start();
        if(isset($_SESSION['id'])){
            $id =  $_SESSION["id"];
            $_SESSION['id_user_posts'] = $id; 
            $u = UsuarioDAO::searchById($id);
            $user_n      = $u->getUserName();
            $user        = $u->getNome();
            $sobrenome   = $u->getSobrenome();
            $mat         = $u->getMatricula();
            $img         = $u->getImg();
            $serie       = $u->getSerie();
            $curso       = $u->getCurso();
            $status      = $u->getStatus();
        }else{
            header("Location: /iforum/login");
        }
        $cadegs = array('informatica','eletronica','administracao',
                        'fisica','quimica','biologia','matematica',
                        'linguagens','geografia','historia'
                        ,'filosofia','sociologia'); 
        if(!in_array($_GET['categoria'], $cadegs)){
            header("Location:/iforum/error");    
        }
    ?>
<!DOCTYPE html>
    <html>
        <head>
            <!-- TAG DO CHROME MOBILE -->
            <meta name="theme-color" content="#52906F">
            <!-- TAG DO CHROME MOBILE -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
            <title><?= $user." ".$sobrenome ?></title>
            <link rel="stylesheet" href='/iforum/res/lib/bootstrap/css/bootstrap.min.css'>
            <link rel="stylesheet" href='/iforum/res/lib/material-icons/css/materialdesignicons.min.css'>
            <link rel="stylesheet" href='/iforum/res/css/main.css'>
        </head>        
        <body onload="loadForuns('<?= $_GET['categoria'] ?>')">
            <!-- MENU -->
            <?php include ($root."/iforum/templates/navbar.php"); ?>
            <!-- MENU -->  
            
            <div class="col-md-2" style="margin-top:200px;">
                <center>
                    <!-- <img src="/iforum/res/imgs/ding.png" height="88" width="88" alt=""> -->
                    <i class='mdi mdi-forum' style="font-size: 200px; color:#DDD; text-shadow: 0 0 1px #999 "></i>
                </center>
            </div>
            
            <div class="col-md-8 categorias dashboard fix-padding-error">
			    <div class='foruns'>
                        
                 </div>	    
            </div>
            <!-- CHAT -->
            <?php include_once '../templates/chat.php'; ?>
            <!-- CHAT -->
            <!-- janelinhas -->
            <div id="janelas"><!--JANELAS CARREGARÃO AQUI... --></div>
            <!-- janelinhas -->
        

        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>        -->
        <script type="text/javascript" src="/iforum/res/lib/jquery/jquery.all.js"></script>
        <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/iforum/res/js/usr_p.js"></script>
        <script type="text/javascript" src="/iforum/res/js/chat.js"></script>   
        <script type="text/javascript" src="/iforum/res/js/play_sound.js"></script>
        <script> 
            $('body').on('click','.forum',function(){
                window.location.href = "/iforum/salas/"+$(this).attr('id');
            });
        </script>
        </body>   
    </html>
