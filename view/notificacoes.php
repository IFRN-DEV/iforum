<?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        require_once ($root."/iforum/DAO/UsuarioDAO.php");
        require_once ($root."/iforum/model/Usuario.php");

        /*$url  = $_SERVER['REQUEST_URI'];$user = explode("/",$url)[2];*/
        $user = $_GET['user'];
        session_start();
        if(isset($_SESSION['id'])){
            if($user == $_SESSION['userName']){

                $id = $_SESSION["id"];
                $_SESSION['id_user_posts'] = $id;
                $u = UsuarioDAO::searchById($id);
                $nome        = $u->getNome();
                $sobrenome   = $u->getSobrenome();
                $email       = $u->getEmail();
                $mat         = $u->getMatricula();
                $img         = $u->getImg();
                $serie       = $u->getSerie();
                $curso       = $u->getCurso();
                $status      = $u->getStatus();
            }else{
                $u = UsuarioDAO::searchByUserName($user);
                if($u != null){
                    $id          = $u->getId();
                    $nome        = $u->getNome();
                    $sobrenome   = $u->getSobrenome();
                    $email       = $u->getEmail();
                    $mat         = $u->getMatricula();
                    $img         = $u->getImg();
                    $serie       = $u->getSerie();
                    $curso       = $u->getCurso();
                    $status      = $u->getStatus();
                    $_SESSION['id_user_posts'] = $id;
                }else{
                      header("Location:/iforum/error");
                }
            }
        }else{
            header("Location:/iforum/login");
        }
        $isMe = $id == $_SESSION['id'];
    ?>
<!DOCTYPE html>
    <html>
        <head>
            <!-- TAG DO CHROME MOBILE -->
            <meta name="theme-color" content="#52906F">
            <!-- TAG DO CHROME MOBILE -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
            <title>Notificações</title>
            <link rel="stylesheet" href='/iforum/res/lib/bootstrap/css/bootstrap.min.css'>
            <link href="/iforum/res/lib/material-icons/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" href='/iforum/res/css/main.css'>
        </head>
            <!-- CHANGE THE PHOTO -->
             <div id="changePhoto" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Mude sua foto de perfil </h4>
                  </div>
                    <div class="modal-body">
                        <h5>
                            <center>
                                <img src="<?=$img?>" width="200px" height="200px" class='img-circle preview'>
                            </center>
                        </h5>
                       <br><br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn cancel-btn cancel-upload-photo' data-dismiss='modal'>Cancelar</button>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                        <div class="imgUpload btn btn-default">
                                            <span style='font-size:14px'>Escolher</span>
                                            <form action="/iforum/controller/usuario.php" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="UPLOAD_IMG" >
                                                <input type="hidden" name="id_user"  value="<?=$id?>">
                                                <input name="img" id="user_img_select"  type="file" accept="image/*" onchange="this.form.submit()"/>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        <body >
            <!-- MENU -->
            <?php include ($root."/iforum/templates/navbar.php"); ?>
            <!-- MENU -->
            
            <!-- PROFILE BAR -->
            <?php include $root."/iforum/templates/profileBar.php"; ?>   
            <!-- PROFILE BAR -->
            

                <!-- DASHBOARD -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 dashboard fix-padding-error">
                    <div class="dashboard-publish">
                    <center><i style='color:#E0E0E0;text-shadow:0 -1px #BBB; font-size:100px; ' class="mdi mdi-bell" data-name="mdi-bell"></i></center>
                    <p class="subtitle" style='margin:10px 0 50px 0;color:#AAA'>Notificações</p>
                     <div>
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs notifications" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#friends" aria-controls="friends" role="tab" data-toggle="tab">
                                <i style='color:#2FA070;font-size:22px; margin-right:3px;' class="pull-right img-circle mdi mdi-account-plus"></i>
                                <p id='friends-title' style='font-size:14px'>Solicitações</p>
                            </a>
                        </li>

                        <li role="presentation">
                            <a href="#comentarios" aria-controls="comentarios" role="tab" data-toggle="tab">
                                <i style='color:#2FA070;font-size:22px; margin-right: 3px' class='pull-right mdi mdi-comment-multiple-outline img-circle'></i> 
                                <p id='forums-title'  style='font-size:14px'>Fóruns</p>
                            </a>
                        </li>
                      </ul>
                      <!-- Tab panes -->
                     <div class="tab-content box tab-notifications">
                        <!-- TAB FRIENDS -->
                        <div role="tabpanel" class="tab-pane active" id="friends">
                          <div class="friends-top">

                          </div>
                          <ul id="friends-requests">
                                <br><br>
                                <center><small>Carregando...</small></center>
                           </ul>
                        </div>

                        <!-- TAB COMMENTS -->
                        <div role="tabpanel" class="tab-pane" id="comentarios">
                             <div class="forums-top">
                                
                             </div>
                          <ul id="forums" >
                                <br><br>
                                <center>
                                    <i style='margin-bottom:10px; font-size:40px; color:#DDD' class='mdi mdi-comment-multiple-outline'></i>
                                    <br>
                                    <small style='font:15px Roboto'>Tudo limpo aqui...</small>
                                </center> 
                           </ul>
                        </div>
                      </div>

                    </div>
                    </div>
                </div>
                <!-- DASHBOARD -->


            <!-- CHAT --><?php include_once '../templates/chat.php'; ?><!-- CHAT -->      
            <!-- janelinhas --><div id="janelas"><!--JANELAS CARREGARÃO AQUI... --></div><!-- janelinhas -->
            <div class="notification-bar"><div class="notification-pin"><!-- load the notifications here... --></div></div>

        <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.js"></script>-->
        <script type="text/javascript" src="/iforum/res/lib/jquery/jquery.all.js"></script>
        <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/iforum/res/js/usr_p.js"></script>
        <script type="text/javascript" src="/iforum/res/js/chat.js"></script>
        </body>
    </html>
