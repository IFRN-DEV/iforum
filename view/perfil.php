<?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        require_once ($root."/iforum/DAO/UsuarioDAO.php");
        require_once ($root."/iforum/model/Usuario.php");
        require_once ($root."/iforum/controller/Utilitarios.php");

        /*$url  = $_SERVER['REQUEST_URI'];$user = explode("/",$url)[2];*/
        $user = $_GET['user'];

        session_start();
        if(isset($_SESSION['id'])){
            if($user == $_SESSION['userName']){
                $id = $_SESSION["id"];
                $_SESSION['id_user_posts'] = $id;
                $u = UsuarioDAO::searchById($id);

                $nome        = $u->getNome();
                $sobrenome   = $u->getSobrenome();
                $telefone    = $u->getTelefone();
                $cidade      = $u->getCidade();
                $sexo        = $u->getGenero();
                $email       = $u->getEmail();
                $dia         = $u->getDia();
                $mes         = $u->getMes();
                $mes         = $u->getMes();
                $ano         = $u->getAno();
                $mat         = $u->getMatricula();
                $img         = $u->getImg();
                $serie       = $u->getSerie();
                $curso       = $u->getCurso();
                $status      = $u->getStatus();
                $noob        = $u->getNoob();
                $bio         = $u->getBio();
                $link        = $u->getLink();
            }else{
                $u = UsuarioDAO::searchByUserName($user);
                if($u != null){
                    $id          = $u->getId();
                    $nome        = $u->getNome();
                    $sobrenome   = $u->getSobrenome();
                    $telefone    = $u->getTelefone();
                    $cidade      = $u->getCidade();
                    $sexo        = $u->getGenero();
                    $email       = $u->getEmail();
                    $dia         = $u->getDia();
                    $mes         = $u->getMes();
                    $mes         = $u->getMes();
                    $ano         = $u->getAno();
                    $mat         = $u->getMatricula();
                    $img         = $u->getImg();
                    $serie       = $u->getSerie();
                    $curso       = $u->getCurso();
                    $status      = $u->getStatus();
                    $noob        = $u->getNoob();
                    $bio         = $u->getBio();
                    $link        = $u->getLink();
                    
                    $_SESSION['id_user_posts'] = $id;
                }else{
                    header("Location:/iforum/error");
                }
            }
        }else{
            header("Location: /iforum/login");
        }
        $isMe = $id == $_SESSION['id'];
    ?>
<!DOCTYPE html>
    <html>
        <head>
            <!-- TAG DO CHROME MOBILE -->
            <meta name="theme-color" content="#00AA70">
            <!-- TAG DO CHROME MOBILE -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
            <title><?= $nome." ".$sobrenome?></title>
            <link rel="stylesheet" href='/iforum/res/lib/bootstrap/css/bootstrap.min.css'>
            <link rel="stylesheet" href='/iforum/res/lib/material-icons/css/materialdesignicons.min.css'>
            <link rel="stylesheet" href='/iforum/res/css/main.css'>
        </head>

        <!-- MAIN -->
             <div id="main" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Bem vindo </h4>
                  </div>
                    <div class="modal-body">
                        <h5>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit alias doloribus delectus,
                            repudiandae tenetur saepe, dolore animi possimus
                             similique fugiat ipsa atque accusamus illo maxime quas cupiditate asperiores obcaecati odit!
                        </h5>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn btn-default' data-dismiss='modal'>Ok</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- CHANGE THE PHOTO -->
             <div id="changePhoto" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class='close-icon' aria-hidden="true">&times;</span>
                      </button>
                      <h4 class="modal-title"> Mude sua foto de perfil </h4>
                  </div>
                    <div class="modal-body">
                        <h5>
                            <center>
                                <img src="<?=$img?>" width="200px" height="200px" class='img-circle preview'>
                            </center>
                        </h5>
                       <br><br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn cancel-btn cancel-upload-photo' data-dismiss='modal'>Cancelar</button>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                        <div class="imgUpload btn btn-default">
                                            <span style='font-size:14px'>Escolher</span>
                                            <form action="/iforum/controller/usuario.php" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="UPLOAD_IMG" >
                                                <input type="hidden" name="id_user"  value="<?=$id?>">
                                                <input name="img" id="user_img_select"  type="file" accept="image/*" onchange="this.form.submit()"/>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


              <!-- DELETE POST MODAL -->
             <div id="areusure" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Tem certeza ? </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <button class='cancel-btn btn' data-dismiss="modal">Não</button>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <button id='yes' class='btn-default main-btn btn btn-danger pull-right' data-dismiss="modal" autofocus >Sim</button>
                        </div>
                    </div>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


            <body class="perfil">
            <!-- MENU -->
            <?php include ($root."/iforum/templates/navbar.php"); ?>
            <!-- MENU -->
            
            <!-- PROFILE BAR -->
            <?php include $root."/iforum/templates/profileBar.php"; ?>   
            <!-- PROFILE BAR -->
                
                    <!-- DASHBOARD -->
                    <div class="col-md-8 col-lg-8 dashboard profile fix-padding-error">
                        <div class="dashboard-publish">
                            <?php if ($isMe){?>
                            <div class="row edit-profile">
                                <div><i title='Editar perfil' class='pull-right mdi mdi-pencil edit-profile-data'></i></div>
                                <br><br>
                            </div>
                            <?php } ?>
                            <div class="row data">
                                <div class="box personal-data">
                                   <div class="row">
                                        <h5>Dados pessoais</h5>
                                        <br>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            
                                            <i class='mdi mdi-account'></i> 
                                            <?= $nome ?> <?= $sobrenome?><br><br>

                                            <i class='mdi mdi-map-marker-radius'></i>
                                            <a target="_blank" href="https://maps.google.com/maps/place/<?=$cidade?>"><?= $cidade ?></a><br><br>

                                            <i class='mdi mdi-email'></i> <?= $email ?>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <?php if($sexo == "m"){?>
                                                <i   class='mdi mdi-gender-male'></i> Masculino<br><br>
                                            <?php }else if($sexo == "f"){ ?>
                                                <i   class='mdi mdi-gender-female'></i> Feminino <br><br>
                                            <?php }else{ ?>
                                                <i   class='mdi mdi-gender-transgender'></i> Outro <br><br>
                                            <?php } ?>
                                            <i class='mdi mdi-cake-variant'></i> <?= $dia." de ".Utilitarios::getMonthFullName($mes).", ".$ano ?><br><br>
                                            <i class='mdi mdi-cellphone'></i> <?= $telefone ?><br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="box student-data">
                                    <div class="row">
                                        <h5> Estudante </h5>
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <i class='mdi mdi-school'></i> <?= $curso ?> 
                                                    <br><br>
                                                    <i class='mdi mdi-account-key'></i>
                                                    <?= $mat ?>
                                                    <br><br>
                                                </div>  
                                                <div class="col-md-6 ">
                                                    <i class='mdi mdi-spellcheck'></i>
                                                    <?= $serie." ano" ?>
                                                    <br><br>
                                                    <i class='mdi mdi-link-variant'></i>
                                                    <?php if($link == 'Website / Rede Social'){$anchor = "";}else{ $anchor = $link; }?>
                                                    <small style='font-size:15px;'><a href="<?= $anchor ?>" title="Website / Rede Social"><?= $link ?></a></small>
                                                    <br><br>
                                                </div>  
                                            </div>
                                            <div class="row">
                                                <br>
                                                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <i class='mdi mdi-book'></i> Biografia: <br><br>
                                                </div>
                                                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <i><small style='font-size:15px; color:black'><?= $bio ?> </small></i>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isMe){?>
                                   <div class="box private-data">
                                        <div class="row">
                                            <h5>Segurança</h5>
                                            <br>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <button id='change-user-pass' class='btn btn-default'>Mudar senha</button>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <button id="erase-all-my-data" class='btn btn-danger'>Excluir Conta</button>
                                            </div>
                                        </div>
                                    </div>
                                <?php  }?>
                            </div>
                        </div>
                    </div>
                    <!-- DASHBOARD -->
                    <!-- CHAT -->
                    <?php include_once '../templates/chat.php'; ?>
                    <!-- CHAT -->
                    <div id="janelas">
                        <!--JANELAS CARREGARÃO AQUI... -->
                    </div>
                    
                    <div class="notification-bar">
                        <div class="notification-pin">
                            <!-- load the notifications here... -->
                        </div>
                    </div>
                
                <script type="text/javascript" src="/iforum/res/lib/jquery/jquery.all.js"></script>
                <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="/iforum/res/js/usr_p.js"></script>
                <script type="text/javascript" src="/iforum/res/js/chat.js"></script>
                <script type="text/javascript" src="/iforum/res/js/play_sound.js"></script>
                    
                <?php if(!$isMe){ ?>
                <script> checkFollow('<?= $_SESSION['id'] ?>','<?= $id ?>'); </script>
                <?php } ?>
                 <!-- LAUNCH MODAL -->
                <?php if($isMe && $noob == 0){ ?>
                <script> $('#main').modal('show'); </script>
                <?php UsuarioDAO::updateNoob($id);}?>
                <!-- LAUNCH MODAL -->
               <!-- SHOW PREVIEW - UPLOAD PHOTO -->
               <script>
                   $("#user_img_select").change(function(e) {
                        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                            var file = e.originalEvent.srcElement.files[i];
                            var reader = new FileReader();
                            reader.onloadend = function() {;
                                $(".preview").attr("src", reader.result);
                                $(".preview").css("width","200");
                                $(".preview").css("height","200");
                            }
                            reader.readAsDataURL(file);
                        }
                       /*$('#areusure').modal('show');
                        $("body").on('click','#yes',function(){
                            $.ajax({
                                url:"/iforum/pages/ajax_pages/usr_m.php",
                                type:"file",
                                data:{action:"changePhoto",photo:photo},
                                success:function(resp){
                                    alert(resp);
                                }
                            });
                        });*/
                   });
                   $('body').on('click','.cancel-upload-photo',function(){$('.preview').attr('src',"<?= $img ?>");});
                </script>
        </body>
    </html>
