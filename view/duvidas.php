<?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        require_once ($root."/iforum/DAO/UsuarioDAO.php");
        require_once ($root."/iforum/model/Usuario.php");
        require_once ($root."/iforum/controller/Utilitarios.php");

        /*$url  = $_SERVER['REQUEST_URI'];$user = explode("/",$url)[2];*/
        $user = $_GET['user'];

        session_start();
        if(isset($_SESSION['id'])){
            if($user == $_SESSION['userName']){
                $id = $_SESSION["id"];
                $_SESSION['id_user_posts'] = $id;
                $u = UsuarioDAO::searchById($id);

                $nome        = $u->getNome();
                $sobrenome   = $u->getSobrenome();
                $telefone    = $u->getTelefone();
                $cidade      = $u->getCidade();
                $sexo        = $u->getGenero();
                $email       = $u->getEmail();
                $dia         = $u->getDia();
                $mes         = $u->getMes();
                $mes         = $u->getMes();
                $ano         = $u->getAno();
                $mat         = $u->getMatricula();
                $img         = $u->getImg();
                $serie       = $u->getSerie();
                $curso       = $u->getCurso();
                $status      = $u->getStatus();
                $noob        = $u->getNoob();
            }else{
                $u = UsuarioDAO::searchByUserName($user);
                if($u != null){
                    $id          = $u->getId();
                    $nome        = $u->getNome();
                    $sobrenome   = $u->getSobrenome();
                    $telefone    = $u->getTelefone();
                    $cidade      = $u->getCidade();
                    $sexo        = $u->getGenero();
                    $email       = $u->getEmail();
                    $dia         = $u->getDia();
                    $mes         = $u->getMes();
                    $mes         = $u->getMes();
                    $ano         = $u->getAno();
                    $mat         = $u->getMatricula();
                    $img         = $u->getImg();
                    $serie       = $u->getSerie();
                    $curso       = $u->getCurso();
                    $status      = $u->getStatus();
                    $noob        = $u->getNoob();

                    $_SESSION['id_user_posts'] = $id;
                }else{
                    header("Location:/iforum/error");
                }
            }
        }else{
            header("Location: /iforum/login");
        }
        $isMe = $id == $_SESSION['id'];
    ?>

<!DOCTYPE html>
    <html>
        <head>
            <!-- TAG DO CHROME MOBILE -->
            <meta name="theme-color" content="#2FAF70">
            <!-- TAG DO CHROME MOBILE -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
            <title><?= $nome." ".$sobrenome?></title>
            <link rel="stylesheet" href='/iforum/res/lib/bootstrap/css/bootstrap.min.css'>
            <link rel="stylesheet" href='/iforum/res/lib/material-icons/css/materialdesignicons.min.css'>
            <link rel="stylesheet" href='/iforum/res/css/main.css'>
        </head>
        
            <!-- CHANGE THE PHOTO -->
             <div id="changePhoto" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Mude sua foto de perfil </h4>
                  </div>
                    <div class="modal-body">
                        <h5>
                            <center>
                                <img src="<?=$img?>" width="200px" height="200px" class='img-circle preview'>
                            </center>
                        </h5>
                       <br><br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn cancel-btn cancel-upload-photo' data-dismiss='modal'>Cancelar</button>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                        <div class="imgUpload btn btn-default">
                                            <span style='font-size:14px'>Escolher</span>
                                            <form action="/iforum/controller/usuario.php" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="UPLOAD_IMG" >
                                                <input type="hidden" name="id_user"  value="<?=$id?>">
                                                <input name="img" id="user_img_select"  type="file" accept="image/*" onchange="this.form.submit()"/>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


              <!-- DELETE POST MODAL -->
             <div id="areusure" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Tem certeza ? </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <button class='cancel-btn btn' data-dismiss="modal">Não</button>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <button id='yes' class='btn-default main-btn btn btn-danger pull-right' data-dismiss="modal" autofocus >Sim</button>
                        </div>
                    </div>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

              <!-- FRIENDS LIKED MODAL -->
             <div id="peopleLikedModal" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Quem gostou: </h4>
                  </div>
                  <div class="modal-body">
                    <div class='listPeople'>
                        
                        <!-- CARREGAR AS PESSOAS AQUI... -->
                    </div>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            

            <body id='body-forum'>
            <!-- MENU -->
            <?php include ($root."/iforum/templates/navbar.php"); ?>
            <!-- MENU -->
          
            <!-- PROFILE BAR -->
            <?php include $root."/iforum/templates/profileBar.php"; ?>   
            <!-- PROFILE BAR -->
                
               <!-- DASHBOARD -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8  dashboard-main fix-padding-error">
                    <?php if($isMe){ ?>
                    <div class="dashboard-publish">
                        <form>
                            <input type="hidden" name="id_user" id="id_user" value="<?=$_SESSION['id']?>">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <textarea  name="duvida" id="duvida" class="publish"  placeholder="Qual a sua dúvida?" data-autoresize row="5" data-placement="bottom" data-toggle="popover"  data-content="Me parece que você está tentando publicar um fórum em branco... :("></textarea>
                                </div>
                                <div class="panel-footer">

                                    <ul class="nav nav-pills">
                                        <li>
                                            <span style='color:#888; font-size:35px' class="mdi mdi-camera"></span>
                                        </li>
                                        <li class="pull-right">
                                            <span class="invisible">b</span>
                                            <input  type="button" id="btn-publicar" value="Publicar" class="pull-right">
                                        </li>
                                        <li class="pull-right">

                                            <select name="categs"  id="categs" class="pull-right" data-placement="bottom" data-toggle="popover"  data-content="Por favor, escolha uma categoria na qual sua dúvida se encacha..." >
                                                <option value="">Categorias</option>
                                                <option value="informatica">Informática</option>
                                                <option value="eletronica">Eletrônica</option>
                                                <option value="administracao">Administração</option>
                                                <option value="fisica">Física</option>
                                                <option value="quimica">Química</option>
                                                <option value="biologia">Biologia</option>
                                                <option value="matematica">Matemática</option>
                                                <option value="linguagens">Linguagens</option>
                                                <option value="geografia">Geografia</option>
                                                <option value="historia">História</option>
                                                <option value="filosofia">Filosofia</option>
                                                <option value="sociologia">Sociologia</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                              </div>
                        </form>
                    </div>
                    <hr class="divider">
                     <?php } ?>
                    <div class="posts">
                        <!-- Carregar postagens aqui -->
                        <br>
                        <?php include $root.'/iforum/pages/ajax_pages/preload.html'; ?>
                    </div>
                </div>
                <!-- DASHBOARD -->

               <!-- CHAT -->
	            <?php include_once '../templates/chat.php'; ?>
	            <!-- CHAT -->
	            <div id="janelas">
	                <!--JANELAS CARREGARÃO AQUI... -->
	            </div>
                
               	<div class="notification-bar">
                    <div class="notification-pin">
                        <!-- load the notifications here... -->
                    </div>
                </div>
        <!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->         
        <script type="text/javascript" src="/iforum/res/lib/jquery/jquery.all.js"></script>
        <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/iforum/res/js/usr_p.js"></script>
        <script type="text/javascript" src="/iforum/res/js/chat.js"></script>
        <script type="text/javascript" src="/iforum/res/js/play_sound.js"></script>
                
        <?php if(!$isMe){ ?>
        <script> checkFollow('<?= $_SESSION['id'] ?>','<?= $id ?>'); </script>
        <?php } ?>
        <!-- TO RISIZE THE TEXTAREA -->
        <script>
            $.each($('textarea[data-autoresize]'),function(){
                var offset = this.offsetHeight - this.clientHeight;
                var resizeTextarea = function(el) {
                    $(el).css('height', 'auto').css('height',el.scrollHeight + offset);
                };
                $(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
            });
        </script>
        <!-- TO RISIZE THE TEXTAREA -->        
        </body>
    </html>
