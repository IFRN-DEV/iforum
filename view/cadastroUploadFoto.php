<!DOCTYPE html>
<html>
    <head>
        <title>Cadastro</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" href="/iforum/res/lib/bootstrap/css/bootstrap.min.css">  
        <link rel="stylesheet" href="/iforum/res/css/main.css">
        <script src='/iforum/res/lib/jquery/jquery.all.js'></script>
    </head>
    <body>
        <?php 
            if(isset($_POST['UPLOAD_IMG'])){
                $id = $_POST['id_user'];
            }else{
                header("Location: /iforum/login");
            }
         ?>
        <div class="container"><!-- Conteúdo_da_Página --> 
            <div class="row"><!-- Linha(Divisão_em_Colunas) -->
                <h1 class="title">iForum</h1><!-- Título -->
                <h3 class="subtitle"> Selecione um foto para o seu perfil </h3><!-- Subtítulo -->
                <div class="col-md-4"></div><!-- Coluna_Encher-Linguiça -->
                <div class="col-md-4"><!-- Coluna_Formulário --> 
                    <div class="box"><!-- Caixinha_Cadastro --> 
                            <!-- Form_UPLOAD de IMAGEM -->
                            <form id="form-login" action="/iforum/controller/usuario.php" method="POST" enctype="multipart/form-data">
                                <center>
                                    <input id="img_upload" name="img" type="file" class="hide" accept="image/*">
                                    <img id="preview" src="/iforum/res/imgs/user.png" onclick="$('input[type=file]').click()"  class="img-circle"  id="user-login"  height="200" width="200" style="cursor:pointer">    
                                    <br>
                                    <p>Clique na imagem para fazer o upload da sua imagem</p>
                                    <br><br>
                                </center>
                                <input type="hidden"  name="id_user"      value="<?= $id ?>"  class="form-control success" placeholder="Senha" required>
                                <input type="hidden"  name="UPLOAD_IMG" class="btn btn-success" value="Salvar"><!-- Button_Entrar -->
                                
                                <a href="/iforum/login"><input style='width:100%'type="button" name="btn_login" class="btn btn-danger" value="Pular"></a><!-- Button_Sou-Novo -->
                            </form>

                            <!-- Fim_do_Form_Login --> 
                    </div><!-- Fim_da_Caixinha_Cadastro -->   
                </div><!-- Coluna_Formulário -->  
                
                <div class="col-md-4"></div><!-- Coluna_Encher-Linguiça -->                
            </div><!-- Fim_da_Linha(Divisão_em_Colunas) -->    
        </div><!-- Conteúdo_da_Página -->
        
         <script>
                $('#img_upload').change(function(){
                    var img = $(this).val();
                    if(img != ""){
                        $('#form-login').submit();
                    }                                          
                });
        </script>
        
        
    </body>
</html>