<?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        require_once ($root."/iforum/DAO/UsuarioDAO.php");
        require_once ($root."/iforum/model/Usuario.php");
        require_once ($root."/iforum/controller/Utilitarios.php");

        /*$url  = $_SERVER['REQUEST_URI'];$user = explode("/",$url)[2];*/
        $user = $_GET['user'];

        session_start();
        if(isset($_SESSION['id'])){
            if($user == $_SESSION['userName']){
                $id = $_SESSION["id"];
                $_SESSION['id_user_posts'] = $id;
                $u = UsuarioDAO::searchById($id);

                $nome        = $u->getNome();
                $sobrenome   = $u->getSobrenome();
                $telefone    = $u->getTelefone();
                $cidade      = $u->getCidade();
                $sexo        = $u->getGenero();
                $email       = $u->getEmail();
                $dia         = $u->getDia();
                $mes         = $u->getMes();
                $mes         = $u->getMes();
                $ano         = $u->getAno();
                $mat         = $u->getMatricula();
                $img         = $u->getImg();
                $serie       = $u->getSerie();
                $curso       = $u->getCurso();
                $status      = $u->getStatus();
                $noob        = $u->getNoob();
                $bio         = $u->getBio();
                $link        = $u->getLink();
            }else{
                $u = UsuarioDAO::searchByUserName($user);
                if($u != null){
                    $id          = $u->getId();
                    $nome        = $u->getNome();
                    $sobrenome   = $u->getSobrenome();
                    $telefone    = $u->getTelefone();
                    $cidade      = $u->getCidade();
                    $sexo        = $u->getGenero();
                    $email       = $u->getEmail();
                    $dia         = $u->getDia();
                    $mes         = $u->getMes();
                    $mes         = $u->getMes();
                    $ano         = $u->getAno();
                    $mat         = $u->getMatricula();
                    $img         = $u->getImg();
                    $serie       = $u->getSerie();
                    $curso       = $u->getCurso();
                    $status      = $u->getStatus();
                    $noob        = $u->getNoob();
                    $bio         = $u->getBio();
                    $link        = $u->getLink();
                    $_SESSION['id_user_posts'] = $id;
                }else{
                    header("Location:/iforum/error");
                }
            }
        }else{
            header("Location: /iforum/login");
        }
        $isMe = $id == $_SESSION['id'];
        if(!$isMe){
            header("Location: /iforum/login");
        }
    ?>
<!DOCTYPE html>
    <html>
        <head>
            <!-- TAG DO CHROME MOBILE -->
            <meta name="theme-color" content="#2FAF70">
            <!-- TAG DO CHROME MOBILE -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
            <title><?= $nome." ".$sobrenome?></title>
            <link rel="stylesheet" href='/iforum/res/lib/bootstrap/css/bootstrap.min.css'>
            <link rel="stylesheet" href='../res/lib/datepicker/dist/css/bootstrap-datepicker.min.css'>
            <link rel="stylesheet" href='/iforum/res/lib/material-icons/css/materialdesignicons.min.css'>
            <link rel="stylesheet" href='/iforum/res/css/main.css'>
        </head>

        <!-- MAIN -->
             <div id="main" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Bem vindo </h4>
                  </div>
                    <div class="modal-body">
                        <h5>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit alias doloribus delectus,
                            repudiandae tenetur saepe, dolore animi possimus
                             similique fugiat ipsa atque accusamus illo maxime quas cupiditate asperiores obcaecati odit!
                        </h5>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn btn-default' data-dismiss='modal'>Ok</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- CHANGE THE PHOTO -->
             <div id="changePhoto" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Mude sua foto de perfil </h4>
                  </div>
                    <div class="modal-body">
                        <h5>
                            <center>
                                <img src="<?=$img?>" width="200px" height="200px" class='img-circle preview'>
                            </center>
                        </h5>
                       <br><br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn cancel-btn cancel-upload-photo' data-dismiss='modal'>Cancelar</button>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                        <div class="imgUpload btn btn-default">
                                            <span style='font-size:14px'>Escolher</span>
                                            <form action="/iforum/controller/usuario.php" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="UPLOAD_IMG" >
                                                <input type="hidden" name="id_user"  value="<?=$id?>">
                                                <input name="img" id="user_img_select"  type="file" accept="image/*" onchange="this.form.submit()"/>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


              <!-- DELETE POST MODAL -->
             <div id="areusure" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Tem certeza ? </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <button class='cancel-btn btn' data-dismiss="modal">Não</button>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <button id='yes' class='btn-default main-btn btn btn-danger pull-right' data-dismiss="modal" autofocus >Sim</button>
                        </div>
                    </div>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- MAIN -->
             <div id="city" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"> Cidade </h4>
                  </div>
                    <div class="modal-body">
                            <div id="map_city"></div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button class='btn btn-default' data-dismiss='modal'>Ok</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <body>
            <!-- MENU -->
            <?php include ($root."/iforum/templates/navbar.php"); ?>
            <!-- MENU -->
            
            <!-- PROFILE BAR -->
            <?php include $root."/iforum/templates/profileBar.php"; ?>   
            <!-- PROFILE BAR -->
                
            <!-- DASHBOARD -->
            <div class="col-md-8 col-lg-8 dashboard profile fix-padding-error">
                <div class="dashboard-publish">
                    <div class="row">

                        <div class="box ">
                            <div class="row editUserForm">
                                <form action="/iforum/controller/usuario.php" method="post">
                                    <h5>Dados pessoais</h5>
                                    <br>
                                
                                    <b>Usuário</b>
                                    <input autofocus name='userName' type="text" value="<?= $user ?>"><br><br><br>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <!-- NAME -->
                                        <b>Nome</b> 
                                        <input name="nome"  type="text" value="<?= $nome ?>"/><br><br>
                                        <!-- NAME -->
                                    
                                        <!-- CITY -->
                                        <b>Cidade</b>
                                        <input name="city" type="text" value="<?= $cidade ?>"/><br><br>
                                        <!-- CITY -->         
                                        <b>Gênero</b>
                                        <?php if($sexo == "m"){?>
                                            <select name="sexo" ><option value="m">Masculino</option><option value="f">Feminino</option><option value="o">Outro</option></select>
                                        <?php }else if($sexo == "f"){ ?>
                                            <select name="sexo" ><option value="f">Feminino</option><option value="m">Masculino</option><option value="o">Outro</option></select>
                                        <?php }else{ ?>
                                            <select name="sexo" ><option value="o">Outro</option> <option value="m">Masculino</option> <option value="f">Feminino</option></select>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <b>Sobrenome</b> 
                                        <input name="nome" type="text" value=" <?= $sobrenome?>"/><br><br>
                                        
                                        <b>Telefone</b>
                                        <input type="text" value="<?= $telefone ?>"><br><br>

                                        <b>Aniversário</b>
                                        <input id="birthday" type="text" class="form-control" value="<?= "$dia/$mes/$ano" ?>">
                                        <br><br>    
                                    </div>
                                    <!-- EMAIL -->
                                    <b>Email</b><input name="email" type="text" value="<?= $email ?>" /><br><br>
                                    <!-- EMAIL -->
                                    
                                        
                                <h5>Estudante</h5>
                                    <br>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <b>Curso</b> 
                                        <input name="curso"  type="text" value="<?= $curso ?>"/><br><br>
                                        <b>Matricula</b> 
                                        <input name="nome" type="text" value=" <?= $mat?>"/><br><br>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <b> Ano </b>
                                        <input name="serie" type="text" value="<?= $serie ?>"/><br><br>
                                        
                                        <b>Website / Rede Social</b>
                                        <input type="text" value="<?= $link ?>"><br><br>
                                    </div>
                                        <b>Biografia</b>
                                        <textarea name="bio" rows="5" data-autoresize><?= $bio ?></textarea>       
                                    
                                    <br><br><br>
                                    <input type="submit" value="Salvar alterações" class="btn btn-success">      
                                    <input type="reset" value="Cancelar alterações" class="btn btn-danger">      
                                </form>
                                </div>  
                            </div> <!-- BOX END -->
                        
                            </div>
                        </div>
                    </div>            
                </div>
                <!-- DASHBOARD -->
                <!-- CHAT -->
                <?php include_once '../templates/chat.php'; ?>
                <!-- CHAT -->
                <div id="janelas">
                    <!--JANELAS CARREGARÃO AQUI... -->
                </div>
                
                <div class="notification-bar">
                    <div class="notification-pin">
                        <!-- load the notifications here... -->
                    </div>
                </div>
                
                <script type="text/javascript" src="/iforum/res/lib/jquery/jquery.all.js"></script>
                <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="../res/lib/datepicker/dist/js/bootstrap-datepicker.min.js"></script>
                <script type="text/javascript" src="/iforum/res/js/usr_p.js"></script>
                <script type="text/javascript" src="/iforum/res/js/chat.js"></script>
                <script type="text/javascript" src="/iforum/res/js/play_sound.js"></script>
                <script> $("#birthday").datepicker(); </script>
                <!-- TO RISIZE THE TEXTAREA -->
                <script>
                    $.each($('textarea[data-autoresize]'),function(){
                        var offset = this.offsetHeight - this.clientHeight;
                        var resizeTextarea = function(el) {
                            $(el).css('height', 'auto').css('height',el.scrollHeight + offset);
                        };
                        $(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
                    });
                </script>
                <!-- TO RISIZE THE TEXTAREA -->

        </body>
    </html>
