<?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        require_once ($root."/iforum/DAO/UsuarioDAO.php");
        require_once ($root."/iforum/DAO/PostagemDAO.php");
        require_once ($root."/iforum/model/Usuario.php");
        session_start();
        if(isset($_SESSION['id'])){
            $id = $_SESSION["id"];
            $_SESSION['id_user_posts'] = $id;
            $u = UsuarioDAO::searchById($id);
            $user        = $u->getNome();
            $sobrenome   = $u->getSobrenome();
            $mat         = $u->getMatricula();
            $img         = $u->getImg();
            $serie       = $u->getSerie();
            $curso       = $u->getCurso();
            $status      = $u->getStatus();
        }else{header("Location: /iforum/login");}

        $forun = PostagemDAO::searchById($_GET['num']); 
?>
<!DOCTYPE html>
    <html>
        <head>
            <!-- TAG DO CHROME MOBILE -->
            <meta name="theme-color" content="#52906F">
            <!-- TAG DO CHROME MOBILE -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
            <title><?= $user ?></title>
            <link rel="stylesheet" href='/iforum/res/lib/bootstrap/css/bootstrap.min.css'>

            <link rel="stylesheet" href='/iforum/res/lib/material-icons/css/materialdesignicons.min.css'>
            <link rel="stylesheet" href='/iforum/res/css/main.css'>
        </head>
        <body >
            <!-- MENU -->
            <?php include ($root."/iforum/templates/navbar.php"); ?>
            <!-- MENU -->
            <?php if($forun->getCategoria() == $_GET['cat']){ ?>
                <div class="col-md-10 categorias dashboard fix-padding-error">
                      <div class="box <?= $forun->getCategoria() ?>">
                        <?php  $u = UsuarioDAO::searchById($forun->getId_User()); ?>
                        <div class='row'>
                             <div class='col-lg-2 col-md-2 col-sm-2 col-xs-12' >
                                <center><img src='<?= $u->getImg()?>' alt="Foto de <?= $u->getNome() ?>" width="120" height="120" class='img-circle'></center>         
                             </div>
                             <div class='col-lg-10 col-md-10 col-sm-10 col-xs-12'  >
                                <br>
                                <nome style='color:#22AA88; font:20px Roboto; margin-top:-20px;'> <?= $u->getNome()." ".$u->getSobrenome() ?></nome>
                                <br><small style='font-size:13px'> <?= $u->getCurso() ?></small> · <small style='font-size:12px'> <?= $forun->getDataHora() ?></small>         
                                         
                                
                             </div>
                        </div>
                        <hr>
                        <p style='font:19px Roboto;'><?= $forun->getDuvida() ?></p>    
                      </div>
                  </div>
                <?php }else{ ?>
                    <div class="col-md-10 categorias dashboard">
                        <center><small><h1> OCORREU UM ERRO!! <br><br> <img src='/iforum/res/imgs/ding.png'> </h1></small></center>      
                    </div>
                <?php } ?>  

            <!-- CHAT -->
            <?php include_once '../templates/chat.php'; ?>
            <!-- CHAT -->
            
            <!-- janelinhas -->
            <div id="janelas">
                <!--JANELAS CARREGARÃO AQUI... -->
            </div>
            <!-- janelinhas -->



        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>        -->
        <script type="text/javascript" src="/iforum/res/lib/jquery/jquery.all.js"></script>
        <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/iforum/res/js/usr_p.js"></script>
        <script type="text/javascript" src="/iforum/res/js/chat.js"></script>
        <script type="text/javascript" src="/iforum/res/js/play_sound.js"></script>
                
        </body>
</html>
