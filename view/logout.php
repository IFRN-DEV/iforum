<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Saindo...</title>
        <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
    </head>

    <body>
        <p> Aguarde... </p>
        <?php $root= $_SERVER[ 'DOCUMENT_ROOT'];
              require_once ($root."/iforum/DAO/UsuarioDAO.php");
              session_start(); 
              if(isset($_SESSION['id'])){
                  session_destroy();
                  $c= $_GET["code"];
                  UsuarioDAO::updateStatus(0,$c);
                  header("Location: /iforum/login");
              }else {
                  header("Location: /iforum/login");
              } 
        ?>
    </body>

</html>