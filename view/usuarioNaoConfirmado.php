<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title> Usuário não confirmado!! </title>
		<link rel="stylesheet" type="text/css" href="/iforum/res/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/iforum/res/css/main.css">
	</head>
	<body class='userNotConfirmed'>
		<div> 
			<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'></div>
			<div class=' col-lg-4 col-md-4 col-sm-4 col-xs-12'>
				<img src="<?= $_POST['img'] ?>" class='img-circle'><br><br>
				<p class='name'> Olá <?=$_GET['user']?>...</p>
				<p>Você ainda não confirmou sua conta no <a href="">iForum</a>!</p>
				<br><br>
				<div id='passoAPasso'>
					<p> 1 - Vá até sua caixa entrada(Gmail, Hotmail, Yahoo, etc..) </p>
					<br>
					<p> 2 - Encontre o email de confirmação que nós te enviamos </p>
					<br>
					<p> 3 - Clique no botão ou no link para confirmar sua conta</p>
					<br>
					<p> 4 - Aproveite nossos serviços... ;)</p>
				</div>
			</div> 
			<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'></div>
		</div>
		<script src='/iforum/res/lib/jquery/jquery.all.js'></script>
		<script src='/iforum/res/lib/bootstrap/js/bootstrap.min.js'></script>
	</body>
</html>