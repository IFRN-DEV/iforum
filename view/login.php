
<?php 
        session_start();
        if(isset($_SESSION['success_login'])){
            $user = $_SESSION['userName'];
            header("Location:/iforum/$user");
        }
        
 ?>

<!DOCTYPE html>
<html>
    <head>
         <!-- TAG DO CHROME MOBILE -->
        <meta name="theme-color" content="#2FAF70">
        <!-- TAG DO CHROME MOBILE -->
        
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/iforum/res/imgs/favicons/icon.png" sizes="16x16" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="/iforum/res/lib/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/iforum/res/css/main.css">
        <script src="/iforum/res/lib/jquery/jquery.all.js"></script>
        <script type="text/javascript" src="/iforum/res/lib/bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                $('.input-flat').blur(function(){
                    if($(this).val()  !==  '') $(this).addClass('used');
                    else $(this).removeClass('used');
                });
            });
        </script>
    </head>


    <!-- +++++++++MODAL+++++++++ -->
    <?php 
    if(isset($_POST["incorrectUser"])){  
        //<meta name="theme-color" content="#F02E10">  
    ?>
      <div id="incorrect_user" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class='close-icon' aria-hidden="true">&times;</span></button>
                 
                <h4 class="modal-title">Ops!</h4>
            </div>
            <div class="modal-body">
                <h5 style='text-align:left'>Verifique se sua matricula ou senha estão corretas ou se está cadastrado no sistema</h5>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <button class='btn btn-default' data-dismiss='modal'>Ok</button>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
          <script>
                $('#incorrect_user').modal('show');
          </script>
    <?php      
        $incorrectUser = null;
    }?>
    <!-- +++++++++MODAL+++++++++ -->
    
    <body class='body-initial'>
        <div class="container"><!-- Conteúdo_da_Página --> 
            <div class="row"><!-- Linha(Divisão_em_Colunas) -->
                <h1>iForum</h1>
                <h3> Conecte-se, compartilhe suas dúvidas e resolva seus problemas... </h3><!-- Subtítulo -->
                
                <div class="col-md-4"></div><!-- Coluna_Encher-Linguiça -->
                        
                <div class="col-md-4"><!-- Coluna_Formulário --> 
  
                    <div class="box"><!-- Caixinha_Cadastro --> 
                        <center>
                            <img  class="img-circle"  id="user-login" src="/iforum/res/imgs/user.png" height="90" width="90" alt="user icon"><!-- Img_Login -->
                        </center>
                            <form id="form-login" action="/iforum/controller/usuario.php" method="POST">
                                <div class="group">
                                    <input type="text"  name="matricula"  class="input-flat" required autofocus>
                                    <label>Matricula</label>
                                    <span class='bar'></span>
                                </div>
                                <br>
                                <div class="group">    
                                    <input type="password"  name="senha"  class="input-flat" required>
                                    <label>Senha</label>
                                    <span class='bar'></span>
                                    <br>
                                    <a href="">Esqueci minha senha!</a><br><!-- Link_Esqueci-Minha-Senha -->
                                    <br>
                                </div>
                                <input type="submit"    name="Login" class="btn btn-success form" value="Entrar"><!-- Button_Entrar -->
                                <a href="cadastro"><input type="button"    name="btn_login" class="btn btn-danger form pull-right" value="Sou novo"></a><!-- Button_Sou-Novo -->
                            </form>
                            <!-- Fim_do_Form_Login --> 
                    </div><!-- Fim_da_Caixinha_Cadastro -->   
                </div><!-- Coluna_Formulário -->  
                
                <div class="col-md-4"></div><!-- Coluna_Encher-Linguiça -->                
            </div><!-- Fim_da_Linha(Divisão_em_Colunas) -->    
        </div><!-- Conteúdo_da_Página -->
    </body>
</html>
